---
layout: person_page
person_data:
  name: Josh Effengee
  data_name: josh_effengee
  image_path: assets/imgs/people/josh_effengee.png
  title: Internet tough-guy
  bio: 'Podcaster soldier-man and history buff.'
  websites:
    - name: 'The X-Files Podcast by LSG Media'
      url: 'https://www.libertystreetgeek.net/x-files/'
  social:
  roles:
    - show: decipherscifi
      role: guest_cohost
    - show: decipherhistory
      role: guest_cohost
---
