---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-10-18 07:14:07+00:00
description: 'Horror month part 3: The Exorcist! Religion, superstition, demons, devils. Ouija and other avenues. Latin. Split brain. Medical order of operations, more'
layout: podcast_post
permalink: /decipherscifi/the-exorcist-feat-adrian-falcone-internet-nerd-episode-60
redirect_from:
  - /decipherscifi/60
slug: the-exorcist-feat-adrian-falcone-internet-nerd-episode-60
title: 'The Exorcist: demonic possession, skepticism, and horror  w/ Adrian Falcone'
tags:
  - science fiction
  - science
  - religion
  - supernaturalism
  - medicine
  - language
  - linguistics
  - horror

# Episode particulars
image:
  path: assets/imgs/media/movies/the_exorcist_1973.jpg
  alt: Priest standing on a misty night Exorcist movie backdrop
links:
  - text: The Exorcist - Killapalooza
    urls:
      - text: Double Feature Podcast
        url: 'https://doublefeature.fm/2016/killapalooza-28-the-exorcist'
  - text: You Are Two by CGP Grey
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wfYbgdo8e-8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/exorcist-extended-directors/id388512065?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Exorcist-Ellen-Burstyn/dp/B007HVBW2A'
  title: The Exorcist
  type: movie
  year: 1973
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_exorcist_1973.jpg
  number: 60
  guid: 'http://www.decipherscifi.com/?p=1059'
  media:
    audio:
      audio/mpeg:
        content_length: 39763025
        duration: '47:20'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Exorcist_Adrian_Falcone_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/the_exorcist.jpg alt: "Decipher SciFi hosts creepy exorcism with head-spin demon face" %}

#### Editions

Theatrical vs "The Version You've Never Seen."

#### Demons

Devils, demons, pazuzu, deception. How do people get possessed? Different theories.

#### Supernaturalism

Supernatural detective work. Priest losing his faith.

#### "Real" Exorcism

The Catholics. The Rite of Exorcism. Bureaucracy. Serious vs loosey-goosey exorcisms.

#### Simple vs Easy Explanations

Occam's razor. Science.

#### Latin

Tradition. Latin as the language of ancient evil. Ecclesiastical latin.

#### Order of Operations

Medicine -> fringe medicine -> call a priest!

#### Horror

The ways this movie is timelessly excellent and scary. Parenting.

#### Demon Battle Plan

Demons might not be the best at planning, OR we might be inconsequential nothings to them, OR creating a rift between god and his people is their job.
