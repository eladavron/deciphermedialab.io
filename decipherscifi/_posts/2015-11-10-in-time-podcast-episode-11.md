---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-11-10 04:45:58+00:00'
description: 'With In Time on the podcast we talk about biological immortality, bio-pharma DRM, digital currency, time as currency, inflation, social stratification.'
layout: podcast_post
permalink: /decipherscifi/in-time-podcast-episode-11
redirect_from:
  - /decipherscifi/11
slug: in-time-podcast-episode-11
title: 'In Time: immortality, phramaceutical blockchain DRM, and boredom.'
tags:
  - science fiction
  - time
  - economics
  - future
  - dystopia
  - immortality
  - cryptocurrency


# Episode particulars
image:
  path: assets/imgs/media/movies/in_time_2011.jpg
  alt: In Time Justin Timberlake movie backdrop
links:
  - text: Inequality for All
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/inequality-for-all/id721133059?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Inequality-All-Robert-Reich/dp/B00HQAHEQA'
  - text: The Immortalists
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-immortalists/id953630027?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Immortalists-Bill-Andrews/dp/B00U4NIPMI'
  - text: The Economics of In Time
    url: 'http://blog.chewxy.com/2011/11/05/the-economics-of-andrew-niccols-in-time/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/in-time/id476869088?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Time-Justin-Timberlake/dp/B0073PZIYI'
  title: In Time
  type: movie
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 11
  guid: 'http://www.decipherscifi.com/?p=330'
  media:
    audio:
      audio/mpeg:
        content_length: 36113722
        duration: '50:09'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/In_Time_decipherSciFi.mp3'
---
#### Biological Immortality

Telomeres. Telomerease. Garbage collection. Avoiding cancer. _The Immortalists_.

#### Biopharmaceutical DRM

Bitcoin. Digital wealth transfer. Terminator genes. Bio-pharma IP law.

#### Economics and Time Inflation

Wealth and life evaporate with each passing second. This is on top of inflation used on purpose to thin the herd. Time in the film must be only artificially finite.

#### Stratified Society

Geographic and economic lockdown. Banlieue 13.
[youtube]Md30EDh60Us[/youtube]

#### Tech

Skin displays. Biological immortality. Payphones. Electric classic cars.

#### Time Security

Cillian Murphy is lawful good. Minute men. Bad system design: could use personal transfer limits. Arm wrestling.

#### Immortality getting boring

Hoarding of wealth. Artificial scarcity.
