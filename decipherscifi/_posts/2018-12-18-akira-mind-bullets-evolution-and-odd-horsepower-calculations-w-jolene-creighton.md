---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-12-18 07:45:52+00:00
description: 'Akira! Motorcycle vs Roomba. Translation vs localization. Fansub nostalgia. The future of our evolution. Mind bullets. Lasers, terrestrial and space-based.'
layout: podcast_post
permalink: /decipherscifi/akira-mind-bullets-evolution-and-odd-horsepower-calculations-w-jolene-creighton
redirect_from:
  - /decipherscifi/172
slug: akira-mind-bullets-evolution-and-odd-horsepower-calculations-w-jolene-creighton
title: 'Akira: mind bullets, evolution, and odd horsepower calculations w/ Jolene Creighton'
tags:
  - anime
  - lasers
  - manga
  - science
  - science fiction
  - telekenesis
  - vehicles
  - weapons

# Episode particulars
image:
  path: assets/imgs/media/movies/akira_1988.jpg
  alt: Kaneda approaching his bike
links:
  - text: 'Decipher SciFi animated scifi posters'
    urls:
      - text: Twitter
        url: 'https://twitter.com/DecipherSciFi/status/1069233551692967941'
  - text: 'AKIRA: How To Animate Light by Nerdwriter1'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=xf0WjeE6eyM'
media:
  links:
    - text: iTunes
      url: 'https://itunes.apple.com/us/tv-season/akira-original-japanese-version/id752149884'
    - text: Amazon
      url: 'https://www.amazon.com/Akira-English-Dubbed-Katsuhiro-Ohtomo/dp/B00G0A6UU8'
  title: Akira
  type: movie
  year: 1988
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/akira_1988.jpg
  number: 172
  guid: 'https://decipherscifi.com/?p=9061'
  media:
    audio:
      audio/mpeg:
        content_length: '45781263'
        duration: '54:29'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Akira_--_Jolene_Creighton_--_Decipher_SciFi.mp3'
---
#### Akira

Hey this movie was really important! Time and place. Budgets. Omg so pretty.

#### Subtitles

Subtitling adult anime for the west. Translation vs localization. Different industries and approaches to localization. Software localization in comparison to film localization. Fansubs online before the likes of Crunchyroll.

#### Motorcycles

Horsepower! Sweet motorcycles. High-speed motorcycle attacks. Turning people into "mushwater."

#### Funiculars

giant slidey freight elevators. _Also_ mountainside trams. Related words:

* _Funicular_ is Latin for _cord_
* _Funiculus_ is Latin for umbilical cord
* _Umbilicus_ is Latin for navel

#### Waterways

Underwater or otherwise. Rivers vs sewers. The Tokyo sewer and transport overhaul preceding the 1964 Olympics. The inefficiency of poop-sucking truck fleets. Traveling in sewers and dysentery avoidance.

#### Psi powers

Creepy, wrinkly children. Psi floaty chairs, the _escalator_ of chairs! When operational, their ares a floaty chair. When they fail, they are... a comfortable chair.

#### Mind bullets

Obvious thing to do: government research into mind-bullets. Surprising thing: when it works. Even more surprising: when it works so well it blows up Tokyo and starts World War III.

#### The future of humanity

Evolutionary milestones. Latent abilities. Guided evolution. Atavism.

#### Laser guns

Laser rifles! Conventions against blinding weapons. Tricking the enemy into rout with elaborate eyepatches. Recognizing the obvious advantages of space lasers. Space-based light concentrators. The difficulties of penetrating atmosphere with space-based lasers. Space lasers for space junk cleanup ([Kurtzgesagt](https://www.youtube.com/watch?v=yS1ibDImAYU)).

#### Blobby guy

Overloading on mind bullets and turning into a big blobby guy before going all mini-singularity. Crushing your girlfriend in your fleshy bits.
