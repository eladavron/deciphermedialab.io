---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2016-07-12 07:20:09+00:00
description: 'Nick Farmer, creator of the Belter language from The Expanse, joins us to talk about conlanging and language in scifi, The Expanse, and in general.'
layout: podcast_post
permalink: /decipherscifi/language-expanse-feat-nick-farmer-episode-46
redirect_from:
  - /decipherscifi/46
slug: language-expanse-feat-nick-farmer-episode-46
title: 'The Expanse: Nick Farmer on conlanging and the intersection of language and scifi'
tags:
  - science fiction
  - science
  - language
  - linguistics
  - star trek
  - klingon
  - constructed languages
  - the expanse

# Episode particulars
image:
  path: assets/imgs/media/tv/the_expanse_2015_language.jpg
  alt: Two Belter cops standing on a station
links:
  - text: The Art of Language Invention by David J Peterson
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-art-of-language-invention/id965720740?mt=11&at=1001l7hP'
      - text: Amazon
      - url: 'https://www.amazon.com/Art-Language-Invention-Horse-Lords-World-Building-ebook/dp/B00TY3ZMVG'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/the-expanse-season-1/id1059088673?at=1001l7hP&mt=4'
    - text: Amazon
    - url: 'https://www.amazon.com/Dulcinea/dp/B018BZ3UWU'
  title: The Expanse
  type: tv
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: nick_farmer
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/the_expanse_2015_language.jpg
  number: 46
  guid: 'http://www.decipherscifi.com/?p=890'
  media:
    audio:
      audio/mpeg:
        content_length: 38671140
        duration: '01:04:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Language_The_Expanse_Nick_Farmer_decipherSciFi.mp3'
---
#### Conlang

What are constructed languages? Who makes them?

#### The Expanse

Designing the language for the TV show. Designing a posteriori languages and a priori languages and difficulties of each.

#### Slang

A Clockwork Orange, Andrew Niccol, and other uses of slang to flesh out fictional worlds.

#### Pidgins, Creoles

What is a creole, and how did Nick approach designing one for The Expanse
