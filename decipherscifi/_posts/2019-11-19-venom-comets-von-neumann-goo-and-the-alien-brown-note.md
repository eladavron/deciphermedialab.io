---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-11-19 07:45:08+00:00
description: 'Landing on actual irl comets! But not with people yet. MRI sounds. Alien (bowel?) resonant frequencies. Evil Jeff Bezos. Von Neumann goo probe strategies.'
layout: podcast_post
permalink: /decipherscifi/venom-comets-von-neumann-goo-and-the-alien-brown-note
redirect_from:
  - /decipherscifi/217
slug: venom-comets-von-neumann-goo-and-the-alien-brown-note
title: 'Venom: comets, Von Neumann goo, and the alien brown note'
tags:
  - comets
  - comics
  - science
  - science fiction
  - space
  - space travel
  - symbiosis
  - vnon neuman machines

# Episode particulars
image:
  path: assets/imgs/media/movies/venom_2018.jpg
  alt: 'Scary Venom-face'
links:
  - text: 'Bronson'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/bronson/id350327603?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Bronson-Tom-Hardy/dp/B0037VIBGY'
  - text: 'Locke'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/locke/id860902141?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Locke-Tom-Hardy/dp/B00JXD1LDK'
  - text: 'Peaky Blinders'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/peaky-blinders-seasons-1-2/id1054138290?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Episode-1/dp/B017O2MVM6'
      - text: Netflix
        url: 'https://www.netflix.com/title/80002479'
  - text: 'Warrior'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/warrior/id467898112?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Warrior-Tom-Hardy/dp/B006G3MZRQ'
  - text: 'The Dark Knight Rises'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-dark-knight-rises/id567661493?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Dark-Knight-Rises-Christian-Bale/dp/B009LREA1S'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/venom/id1437190744?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Venom-Michelle-Williams/dp/B07J2V85WZ'
  title: Venom
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/venom_2018.jpg
  number: 217
  guid: 'https://decipherscifi.com/?p=10857'
  media:
    audio:
      audio/mpeg:
        content_length: 29602081
        duration: '35:08'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/venom.final.mp3'

---
#### Decontamination protocols

NASA’s irl de-contamination measures. Private space industry without apparent government oversight.

#### Comets

Some of the oldest things in our solar system and maybe the oldest things that we can reach. Leftovers from early solar system formation. The possibility of finding the precursors of life. Delineating between comets and asteroids. Fuzzy definitions. “Extinct” comets. Scales & tails.

#### Von Neumann Goo

Sending out of infected ice-balls and hoping someone notices. Like a tree! Just send your reproductive material everywhere and who cares who it lands on or gives allergies to.

#### Evil Jeff Bezos

Thinly-written villains. Narcissism. Poorly designed humans and “saving” humanity.

#### Sound

And magnets! The frequencies in which MRI machines are loudest, and the way that _doesn’t_ line up with Venom’s sonic sensitivities. Resonance frequencies and alien “[brown notes](https://en.wikipedia.org/wiki/Brown_note).”

#### Symbiosis

Christopher’s surprise learning the distinction between the category of biological interactions that is [symbiosis](https://en.wikipedia.org/wiki/Symbiosis) and its subcategories.
 	
* parasitism
* commensalism
* mutualism (Venom, but perhaps not his compatriots)

{% youtube "https://www.youtube.com/watch?v=b4Li0uOSR58" %}
