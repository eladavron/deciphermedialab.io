---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-05-10 07:45:48+00:00
description: 'With Surrogates on the podcast we talk human connection, prosthetic tech, neural interfaces, robot security measures, genocide, robo-economy, more'
layout: podcast_post
permalink: /decipherscifi/surrogates-movie-episode-37
redirect_from:
  - /decipherscifi/37
slug: surrogates-movie-episode-37
title: 'Surrogates: materials science, pseudonymity, and ludditism'
tags:
  - science fiction
  - science
  - robots
  - virtual reality
  - cyborgs
  - augmented reality

# Episode particulars
image:
  path: assets/imgs/media/movies/surrogates_2009.jpg
  alt: Surrogates Bruce Willis city walk backdrop
links:
  - text: DEKA "Luke" Arm
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=tnjMvme-wDM'
  - text: Game Over by A Life Well Wasted
    urls:
      - text: 99 Percent Invisible
        url: 'http://99percentinvisible.org/episode/game-over/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/surrogates/id346976415?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Surrogates-Bruce-Willis/dp/B005EJNAK4'
  title: Surrogates
  type: movie
  year: 2009
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/surrogates_2009.jpg
  number: 37
  guid: 'http://www.decipherscifi.com/?p=766'
  media:
    audio:
      audio/mpeg:
        content_length: 22321856
        duration: '37:11'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Surrogates_decipherSciFi.mp3'
---
#### Areas of progress

Areas needing a lot of progress to lead to the world of Surrogates: robotics, materials science, network bandwidth and latency, human-machine neural interfaces, legality and ethics.

#### Effects

Violent crime nearly nonexistent, productivity, pseudonymity, anonymity, merito cracy. Level playing field for people with disabilties. Soldiering by proxy.

#### Human connection

The loss of it. The resistance.  Surprisingly bad solutions.
