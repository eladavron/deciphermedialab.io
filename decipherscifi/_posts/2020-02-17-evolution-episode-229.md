---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-02-18 00:02:45-04:00
description: 'Space rock classifications and mining rights. Soft and hard panspermia. Messing with the periodic table. Arsenic and selenium poisoning.'
layout: podcast_post
permalink: /decipherscifi/evolution-panspermia-space-rocks-periodic-tables-and-sooo-much-poison
redirect_from:
  - /decipherscifi/229
slug: evolution-panspermia-space-rocks-periodic-tables-and-sooo-much-poison
title: 'Evolution: panspermia space rocks, periodic tables, and sooo much poison'
tags:
  - aliens
  - chemistry
  - evolution
  - panspermia
  - poison
  - science
  - science fiction
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/evolution_2001.jpg
  alt: 'The cast of Evolution just kind of, leaning.'
links:
  - text: "Matcha! It's so tasty."
    urls:
      - text: 'Wikipedia'
        url: 'https://en.wikipedia.org/wiki/Matcha'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/evolution-2001/id291739684?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Evolution-David-Duchovny/dp/B07C3J418B'
    - text: Netflix
      url: 'https://www.netflix.com/title/60004471'
  title: Evolution
  type: movie
  year: 2001
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/evolution_2001.jpg
  number: 229
  guid: 'https://decipherscifi.com/?p=10935'
  media:
    audio:
      audio/mpeg:
        content_length: 25925203
        duration: '30:45'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/evolution.final.mp3'

---
#### Asteroids etc

Revisiting: asteroids vs meteors vs meteoroids vs meteorites vs who knows. Still confusing! How much space *stuff* hits Earth? Like, not just [dust](https://core.ac.uk/download/pdf/295744.pdf) but things big enough to be impressed by. Who gets claim to Earth-impacting space rocks?

#### Panspermia

The idea that life can spread between worlds. Or maybe even stars (maybe not.)? The "soft hypothesis" of panspermia and the utility of primoridla ooze. The possibility of panspermia bringing life to primordial Earth. Cowboy scientists just leaving alien goo everywhere.

#### Periodic table of the elements

Chemical relationships via the periodic table. [Actually-useful alternative periodic tables](https://en.wikipedia.org/wiki/Periodic_table#Different_priodic_tables). Selenium toxicity, testosterone, and Brazil nuts. Arsenic. Marveling at 19th centtury arsenic popularity.
