---
layout: person_page
person_data:
  name: Cailin Munroe
  data_name: cailin_munroe
  image_path: assets/imgs/people/cailin_munroe.png
  title: 'Producer on The Expanse'
  social:
    - name: twitter
      url: https://www.twitter.com/hellomunroe
    - name: instagram
      url: https://www.instagram.com/cailinmunroe
  roles:
    - show: decipherscifi
      role: guest

---
