---
# General post frontmatter
author: christopher_peterson
categories:
  - irl
date: 2017-02-06 19:31:04+00:00 
layout: post
slug: space-between-us-nyc-movie-meetup-recap 
title: 'The Space Between Us NYC movie meetup trip report'
image:
  path: assets/imgs/misc/meetup_space_between_us_selfie.jpg
  alt: 'The Space Between Us Decipher SciFi NYC meetup group selfie'
tags:
  - selfie

---
Had a great time with everyone who came to hang out and see The Space Between Us!

The tween romance might seem like an odd choice, but you take what you can get when [Chris Noessel]({% link _people/chris_noessel.md %}) is in town and there's only one scifi movie playing. ;)
My favorite part of these things anyway are the conversations afterwards anyway. It's about finding ideas to explore. Had a bunch of good, smart folks who find approaches to things that I'd never think of. And this movie had a bunch of spacey Mars-ey biology physics stuff to take apart. Keep an eye out for an episode on the film, of course. 😃
