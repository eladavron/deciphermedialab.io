---
search_exclude: true
---
{%- assign pages = site.html_pages -%}
{%- assign posts = site.posts -%}
{%- assign collections = site.collections -%}
{%- assign all = pages | concat: posts | concat: collections -%}
const SearchIndex = [
	{%- assign uid = 0 -%}
	{%- for post in all -%}
	  {%- assign content = post.content | strip_html | newline_to_br | strip_newlines | replace: '<br />', ' ' | strip -%}
		{%- if post.search_exclude != true and content != blank -%}
			{
					categories: {{ post.categories | join: ', ' | jsonify }},
					content: {{ content | jsonify }},
					id: {{ uid }},
					links: {{ post.links | map: 'text' | join: ', ' | jsonify }},
					{%- if post.media -%}
						{%- capture media -%}
							{{ post.media.type }}, {{ post.media.year }}, {{ post.media.title }}
						{%- endcapture -%}
					{%- endif %}
					media: {{ media | jsonify }},
					people: {{ post.people | map: 'data_name' | join: ', ' | replace: '_', ' ' | jsonify }},
					permalink: "{{ site.url }}{{ post.url }}",
					tags: {{ post.tags | join: ', ' | jsonify }},
					title: {{ post.title | jsonify }}
			},
			{%- assign uid = uid | plus: 1 -%}
		{%- endif -%}
	{%- endfor %}
];
