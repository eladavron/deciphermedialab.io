---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-10-08 07:45:14+00:00
description: 'The curse of The Terminator. Stepping up VFX. Confusing time travel. Operation Cronos. Ships of bionanite Theseus. Running out of Terminator options. Etc.'
layout: podcast_post
permalink: /decipherscifi/terminator-genisys-surprise-anthologies-time-travel-spaghetti-w-joe-ruppel
redirect_from:
  - /decipherscifi/212
slug: terminator-genisys-surprise-anthologies-time-travel-spaghetti-w-joe-ruppel
title: 'Terminator Genisys: surprise anthologies & time travel spaghetti w/ Joe Ruppel'
tags:
  - arnold schwarzenegger
  - cyborg
  - man versus machine
  - science
  - science fiction
  - skynet
  - terminator
  - time machine
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/terminator_genisys_2015.jpg
  alt: 'Arnold and Daenerys'
links:
  - text: 'Mortal Kombat 11 Kombat Pack – Official Terminator T-800 Gameplay Trailer'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=PCn0r1amyN4'
  - text: 'Scientist Man Explains Terminator: Genisys by Red Letter Media'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=JXJiSZhA5cg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/terminator-genisys/id1001326852?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Terminator-Genisys-Arnold-Schwarzenegger/dp/B017QI003A'
  title: 'Terminator: Genisys'
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/terminator_genisys_2015.jpg
  number: 212
  guid: 'https://decipherscifi.com/?p=10811'
  media:
    audio:
      audio/mpeg:
        content_length: 40891011
        duration: '48:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/terminator_genisys.mp3'

---
#### The curse of the Terminator

Why do all these production companies go under after making these always-profitable Terminator movies?

#### VFX

Twelve months to make a fake Arnold.

{% youtube "https://www.youtube.com/watch?v=DKlbaU_uWpI" %}

{% youtube "https://www.youtube.com/watch?v=kiqIhaCJ5Jc" %}

{% youtube "https://www.youtube.com/watch?v=CTm6YKqz1Og" %}

#### Time travel

Time travel but, like, way more confusing. And possibly nonsensical. Five movie in and we _FINALLY_ we see a time machine. Operation Cronos. Trying to count timelines and getting very confused because they broke everything.

#### Skynet

Skynet in the government shadows vs Silicon Valley public launch. UbiquityOS. Engineering to let the AI our of the box. The dawn of "apps" and how it was important. "Genisys does what Nintendon't." Skynet takeoff scenario, altered yet again.

#### Terminators

A review of the T-3000 and T-5000. Doctor Who. Bio-nanite replacement. Ship of Biorobo Theseus: is the T-3000 still the same person as it was?

#### Kyle Reese

Old Nikes! Scrappy Kyle vs jacked Kyle. David vs Goliath.

> Who wants to go do my mom
> 
> - John Connor

#### Arnold

`build_time_machine.bat` . Time traveling at 1x speed. Various messy strategies for re-encasing your robot arm in flesh.
