---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2020-05-12T00:44:46-04:00
description: "The rest of the season this time. Non-religious philosophy. Synthetic life and galactic evolution. Borg! Mortalty. Etc."
layout: podcast_post
permalink: /decipherscifi/picard-season-1-mortality-great-filters-and-synthetic-life-w-jolene-creighton
redirect_from:
  - /decipherscifi/237
slug: picard-season-1-mortality-great-filters-and-synthetic-life-w-jolene-creighton
title: 'Picard Season 1: mortality, great filters, and synthetic life w/ Jolene Creighton'
tags:
  - artificial intelligence
  - borg
  - fermi paradox
  - great filter
  - mortality
  - nostalgia
  - picard
  - science
  - science fiction
  - simlulation hypothesis
  - star trek

# Episode particulars
image:
  path: assets/imgs/media/tv/picard_2020_season_1.jpg
  alt: 'Picard looking really dapper and heroic on a space ship'
links:
  - text: 'The Fable of the Dragon Tyrant'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=cZYNADOHhVY'
media:
  links:
    - text: 'CBS All Access'
      url: 'https://www.cbs.com/shows/star-trek-picard/'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Trek-Picard/dp/B07S5HX14T'
  title: Picard
  type: tv
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/picard_2020_season_1.jpg
  number: 
  guid: 'https://www.decipherscifi.com/?p=10994'
  media:
    audio:
      audio/mpeg:
        content_length: 39109723
        duration: '46:30'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/picard_season_1.final.mp3'

---
#### Nostalgia

Like all the things with Star Trek in them - feeling nostalgia for good Trek.

#### Prophecy/Not Prophecy

Conquering the [Three Body Problem]({% link decipherscifi/_posts/2020-04-21-three-body-problem-episode-235.md %}) and moving on to the... eight body problem? Breaking your brain. Tidal forces and planetary ejection. Game theory: secret society, or open discussion?

#### Great filters

The great synthetic filter from beyond space and time.

> If that's the words, then that's the words
>
> [Christopher Peterson]({% link _people/christopher_peterson.md %})

#### Synthetic life

Classifying the artificial intelligence of Trek's synthetic life. Issues with anthropocentric terminology. Backing up synthetic "minds." [Finding out you're a robot](https://www.theonion.com/new-study-finds-best-way-to-determine-if-you-are-androi-1819580012) and also, somehow, simulation hypothesis.

#### Mortality

"To die is to be human" and other stupid nonsense. YOLO. "I'm gonna live forever [ha-haaa](https://www.nbc.com/saturday-night-live/video/outrageous-clown-squad-kickspit-dirt-festival/n12792). Wasting deaths in Star Trek.

{% youtube "https://www.youtube.com/watch?v=z5Otla5157c" %}

#### Borg

Small-scale good-guy Borg. Dunbar limits and smaller collectives.
