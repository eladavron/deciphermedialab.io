Migrating posts from wordpress import to Jekyll frontmatter
=================================================================

*NOTE: this documentation from our original wordpress-to-jekyll migration is no longer relevant but remains here because hey why not*

<del>
Simply, put, open the post to be migrated alongside the post template:

```sh
vim -O _docs/templates/episode-decipherscifi.md _posts/decipherscifi/2022-02-32-some-post.md
```

And do up the frontmatter:

1. Copy the template frontmatter into the top of the post
2. Fill out/replace all fields of the template frontmatter that came with the post with their provided values 
  - remove inapplicable categories
  - Use the new, longer, SEO-friendly post titles
  - The "tags" coming from wordpress on old posts are probably way too long and really dumb. Cut all the actors/directors/etc and **keep them lower-case**.
  - The `wordpress_id` field in the wordpress post should not be placed directly into the new post, but rather the *number value* therein should be placed in the end of the `episode:guid` field as `https://www.decipherscifi.com/?p=123`.
    * NOTE: these GUIDs are important as podcatchers in general use them to identify whether something is "new." Thus, if any change when we migrate over from WordPress, people's clients will get bombed with new downloads of old episodes. This happened once already when we went to HTTPS. It wasn't awesome.
3. Fill out the rest of the fields that don't come straight out of wordpress...
  * `Episode Particulars`
    - image path: they should all be there already, and don't forget Vim has local path autocomplete in Insert mode w/ C-X C-F.
    - alt text: desciptive with movie keywords
    - links: Bring up all the links from the post that aren't the actual movie
    - media:links:itunes links if not present can be produced at this page from Apple: [https://linkmaker.itunes.apple.com/en-us/?at=1001l7hP&country=us]
    - media:links:amazon just grab the Amazon streaming url from the post or from Amazon search. Cut all the junk off at this: [http://www.amazon.com/Signs-Mel-Gibson/dp/B0060D18UM]a. I'll create a plugin to manage referral links non-manually later.
    - episode::::content_length can easily be got by a curl command like this one I use by fetching only the header:
      ```sh
      httplen () { 
          curl -L --head "${1}" 2> /dev/null | tr '^M' '\n' | grep --color=auto -P '^Content-Length:' | cut -d ' ' -f 2
      }
      ```
4. Remove any goodbyes or contact info from the bottom - if we want a footer we'll templatize it.

For reference, here is an example of the diff from the source for a post that has already been migrated:

```sh
git diff 413b2c7:./_posts/2016-04-05-signs-movie-podcast-episode-32.md _posts/decipherscifi/fin/2016-04-05-signs-movie-podcast-episode-32.md
```
</del>
