---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-05-21 07:45:30+00:00
description: 'Criticisms & controversy. Mel Gibson. Working from limited written history. The height & decline of Mayan civilization. Mayan science, math, and calendars.'
layout: podcast_post
redirect_from:
  - /decipherscifi/194
slug: 'apocalypto-sacrificial-practices-temples-and-mayan-writing-and-math'
title: 'Apocalypto: sacrificial practices, temples, and Mayan writing and math'
tags:
  - aztec
  - civilizational decline
  - history
  - mathematics
  - maya
  - mesoamerica
  - science
  - writing
  - language

# Episode particulars
image:
  path: assets/imgs/media/movies/apocalypto_2006.jpg
  alt: Apocalypto 2006 movie backdrop
links:
  - text: The Case Study of Apocalypto by Richard D Hansen
    urls:
      - text: ResearchGate
        url: 'https://www.researchgate.net/publication/288187016_Relativism_Revisionism_Aboriginalism_and_EmicEtic_Truth_The_Case_Study_of_Apocalypto'
  - text: Apocalypto by History Buffs
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=U5pBZKj1VnA'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Apocalypto-Blu-ray-Region-Rudy-Youngblood/dp/B000SKAT2Q'
  title: Apocalypto
  type: movie
  year: 2006
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/apocalypto_2006.jpg
  number: 194
  guid: 'https://decipherscifi.com/?p=10408'
  media:
    audio:
      audio/mpeg:
        content_length: 35635970
        duration: '42:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/apocalypto_--_--_decipher_scifi.mp3'

---
#### Criticisms and controversy

So yeah, Mel Gibson is bad. But then the movies are... really good? Trying to get past his framing. The tug of war between historical accuracy and entertainment. Anthropological criticism.

#### Deciphering history

The problem with burning nearly the entire written history of a people. Deciphering scant ancient texts. The remaining resources: art, stelae. Mayan hieroglyphic writing.

#### Periods

The Maya pre-classic, classic, and post-cassic periods. The scale of the height of Mayan civilization. The abruptness of its fall. The period in which the film takes place and the ways in which it is not actually entirely apparent. Classic-period pyramids in the post-classic era.

#### Science and math

The mixing of astronomy and astrology in the Mayan tracking of the heavenly bodies for religious purposes. The Mayan written mathematical system. "Inventing" zero. Extensive calendaring and that whole [2012]({% link decipherscifi/_posts/2017-11-14-2012-feat-fraser-cain-of-universe-today-episode-116.md %}) kerfuffle.

#### Civilizational decline

The factors that may have contributed to the Mayan collapse. Plaster and mortar production. Deforestation for fuel.

#### Aztec/Maya

The possibility for cultural confusion in the film. Aztec and Maya sacrificial practices.
