---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-01-22 07:50:48+00:00
description: "Differing solutions within and without your planet's gravity well. Ion drive airplanes. The weakness of Mars' dust storms. Hypoxia deathbed statements."
layout: podcast_post
permalink: /decipherscifi/2036-origin-unknown-ion-propulsion-dust-storms-and-how-to-speak-greek-in-orbit
redirect_from:
  - /decipherscifi/177
slug: 2036-origin-unknown-ion-propulsion-dust-storms-and-how-to-speak-greek-in-orbit
title: '2036 Origin Unknown: ion propulsion, dust storms, and how to speak Greek in orbit'
tags:
  - hypoxia
  - nuclear power
  - orbit
  - science
  - science fiction
  - space
  - space settlement
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/2036_origin_unknown_2018.jpg
  alt: 'Starbuck looking dramatic in space'
links:
  - text: 'How Do Ion Engines Work? The Most Efficient Propulsion System Out There by Fraser Cain'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=6H0qsqZjLW0'
  - text: 'Why You Should Put YOUR MASK On First (My Brain Without Oxygen) by Smarter Every Day'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=kUfF2MTnqAw'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/81001097'
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/2036-origin-unknown/id1369446184?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/2036-Origin-Katee-Sackhoff/dp/B07CPSCJJP'
  title: 2036 Origin Unknown
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/2036_origin_unknown_2018.jpg
  number: 177
  guid: 'https://decipherscifi.com/?p=9876'
  media:
    audio:
      audio/mpeg:
        content_length: 33430358
        duration: '39:47'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/2036_Origin_Unknown_--_--_Decipher_SciFi.mp3'

---

#### Ion Drive

Compounding acceleration in the relative vacuum of space. Incredible efficiencies versus chemical rockets, and the very low force produced making it only appropriate once spaceborn. The differing problems solved by chemical rockets and ion drives. The benefit of ion drive once out of Earth's gravity well and going far away. Earth's tectonic plates, slowly building up to escape velocity. That beautiful blue glow. "Ionic wind" aircraft in-atmosphere: a completely silent aircraft motor with no moving parts!

{% youtube "https://www.youtube.com/watch?v=CiWb44VRZGo" %}

{% youtube "https://www.youtube.com/watch?v=boB6qu5dcCw" %}

#### Landing in spaaaaace

Methods of landing on Mars: Aeroshells, parachutes, rockets, and balloons. Past examples. Different bodies in space and their particular atmospheric densities, atmospheric conditions, and gravitational acceleration.

#### Nuclear power solutions

Nuclear power vs solar cells. People's presumption of full-on nuclear reactors rather than the smaller and less-efficient radioisotope thermoelectric generators like on [Curiosity](https://en.wikipedia.org/wiki/Curiosity_(rover)). Wondering why we don't use tiny nuclear robots to blow the dust off of the solar robots. NASA, call us for more great ideas.

#### Dust storms

Martian dust storms. The low density and force of Martian dust storms, contrary to popular depictions. The visual impressiveness of huge Martian dust storms from space relative to actual effect. The (low) density of the Martian atmosphere and its make-up. How 60mph dust storms are are actually not that powerful: with low gravitational acceleration, static charge, it is very very easy it is to keep dust particles aloft.

#### Orbital centricity words

The revelation that "geocentric" and "geostationary" apply very specifically to only Earth and there are other words particular to the other major bodies in our solar system. All of the other [centricity classifications](https://en.wikipedia.org/wiki/List_of_orbits#Centric_classifications), including Arecentric for Mars as in the film (though they don't use it).

#### Hypoxia

Hypoxia and inability to give detailed instructions to your AI on how to disperse the spirit of humanity into the universe.
