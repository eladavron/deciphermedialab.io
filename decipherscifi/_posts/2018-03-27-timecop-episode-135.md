---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-03-27 07:40:27+00:00
description: 'Timecop science! Get rich quick slowly time travel schemes, sexy grandfather paradoxes, Van Damme love. Confederate gold, carbon dating, and greebles. Time travel ménage à trois rules. Time loops and more paradoxes. And face-kicking!'
layout: podcast_post
permalink: /decipherscifi/timecop-episode-135
redirect_from:
  - /decipherscifi/135
slug: timecop-episode-135
title: 'Timecop: carbon dating money, confederate gold coins, and time loops'
tags:
  - carbon dating
  - fx
  - paradoxes
  - science
  - science fiction
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/timecop_1994.jpg
  alt: Jean claude and, like, a clock
links:
  - text: Bloodsport
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/bloodsport-1988/id376081721?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Bloodsport-Jean-Claude-Van-Damme/dp/B0032HQ9DU'
  - text: Bloodsport Mentos
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Cw-tceWOL8c'
  - text: You Can't Touch Anything by Vsauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=yE8rkG9Dw4s'
  - text: Can We Really Touch Anything? by Vsauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=bKldI-XGHIw'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/timecop/id479095027?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Timecop-Jean-Claude-Van-Damme/dp/B006GLPQWY'
  title: Timecop
  type: movie
  year: 1994
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/timecop_1994.jpg
  number: 135
  guid: 'https://decipherscifi.com/?p=5347'
  media:
    audio:
      audio/mpeg:
        content_length: 35254101
        duration: '41:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Timecop_--_--_Decipher_SciFi.mp3'
---
#### JCVD

Green screen etc.

{% youtube "https://www.youtube.com/watch?v=4h3a1m4XpAE" %}

{% youtube "https://www.youtube.com/watch?v=yr6cp7cppCc" %}

{% youtube "https://www.youtube.com/watch?v=g_Il5nT2Zpg" %}

#### Time machine carrying capacity

Red smear memorials. Time travel in tiny, sweaty drug subs. It all comes down to laser machine guns.

#### Confederate gold

Get rich quick slowly schemes.

#### Smuggling from the past

Cost of moving mass through time. Burying your pilfered gold for pickup vs time traveling it to the present. Inefficient black market time travel business models.

#### Carbon dating

And how you can't carbon date gold itself.

#### Depression Profiting

Sicilian grandmas. Skyrim health potions and time travel interest-compounding. Better investment plans:

1. Put money into savings account
2. ...
3. profit
4. repeat

#### Making the future

Ugly self-driving cars. [Greebles](https://en.wikipedia.org/wiki/Greeble). Glue some crap to some crap and bam, you're in the future.

#### Timecopping and paradoxes

Time loops. "The same matter can't occupy the same space at the same time." The indistinguishability of atoms of the same isotope. Quantum word soup. "Touching" by neutron star. The grandfather paradox where you bang your grandma. Ted Chiang anti-time loop deciphering advice. It's not gay if you don't touch and as a bonus you also don't explode in paradox.

"[I'm gonna live forever, hahaaaa](https://www.youtube.com/watch?v=DvwBMucHJEY)"
