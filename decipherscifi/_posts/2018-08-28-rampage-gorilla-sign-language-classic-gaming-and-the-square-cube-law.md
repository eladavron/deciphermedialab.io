---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-08-28 07:45:38+00:00
description: 'Rampage game history and scifi trope selection. Zero-G research. CRISPR. Gorilla sign language. Animal fights and the square cube law.'
layout: podcast_post
permalink: /decipherscifi/rampage-gorilla-sign-language-classic-gaming-and-the-square-cube-law
redirect_from:
  - /decipherscifi/157
slug: rampage-gorilla-sign-language-classic-gaming-and-the-square-cube-law
title: 'Rampage: gorilla sign language, classic gaming, and the square cube law'
tags:
  - animals
  - evolution
  - interspecies communication
  - nostalgia
  - retro gaming
  - science
  - science fiction
  - sign language
  - square cube law

# Episode particulars
image:
  path: assets/imgs/media/movies/rampage_2018.jpg
  alt: 'The Rock and like a really really big gorilla'
links:
  - text: 'How to Make an Elephant Explode with Science by Kurtzgesagt'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=MUWUHf-rzks'
  - text: 'Zero-G Experiments on Earth: The Bremen Drop Tower by Tom Scott'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=4aCMDQsx740'
  - text: 'Scientists Successfully Teach Gorilla It Will Die Someday by The Onion'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=CJkWS4t4l0k'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/rampage-2018/id1363164999?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Rampage-Dwayne-Johnson/dp/B07CCGD3MX'
  title: Rampage
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/rampage_2018.jpg
  number: 157
  guid: 'https://decipherscifi.com/?p=7522'
  media:
    audio:
      audio/mpeg:
        content_length: 29890600
        duration: '35:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Rampage_--_--_Decipher_SciFi.mp3'
---
#### Classic arcade gaming

Memories of the arcade and the Sega Game Gear. Introduction of Larry the Rat in the Atari Lynx port. How to pick from current social fears for your lazy game plot.

#### Zero-G research

Genetics research in spaaaaaace. CRISPR. Bremen Drop Tower.

#### Gorilla sign language

Koko the gorilla. Gorilla sign language. Credulity and confirmation bias. Impressive gorilla lexicons. Adventures in signing portmanteus. Yogurt aka "milkfruitcandy food."

#### Animal fights

Comparing the gorilla to the grizzly bear. Relative animal size. Grizzly bears as killing machine. Gorilla conflict-avoidance techniques.

#### Scaling animals

[The square-cube law](https://en.wikipedia.org/wiki/Square%E2%80%93cube_law)! Relative scaling and the race between volume and surface area (volume always wins!). The creation of a bio antenna. The relatively low energy of radio waves.

#### Mutations, mutations, mutations

Gorilla turns into giant gorilla. Lizard turns into mega lizard. Wolf, though, turns into a wolf... with [patagia](https://en.wikipedia.org/wiki/Patagium) and throwable quills! Possibly resembling the urticating hairs of a tarantula.

{% youtube "https://www.youtube.com/watch?v=P9yruQM1ggc" %}
