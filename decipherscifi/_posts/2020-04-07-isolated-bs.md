---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-04-07T02:50:55-04:00
description: 'A conversation in quarantine! The science of ancient sourdough bread. Peanut butter jelly time. Remote learning and remote work. Video games. Etc!'
permalink: /decipherscifi/isolated-bs-coping-with-copious-amounts-of-bread
layout: podcast_post
redirect_from:
  - /decipherscifi/233
slug: isolated-bs-coping-with-copious-amounts-of-bread
title: 'Isolated BS: coping with copious amounts of bread'
tags:

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs.jpg
  alt: Decipher SciFi on acid
links:
  - text: 'Seamus Blackley bakes actual Ancient Egyptian bread'
    urls:
      - text: Twitter
        url: 'https://twitter.com/SeamusBlackley/status/1244470566867726336'
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs.jpg
  number: 233
  guid: 'https://decipherscifi.com/?p=10967'
  media:
    audio:
      audio/mpeg:
        content_length: 28979770
        duration: '34:27'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/isolated_bs.final.mp3'

---
Just a conversation in isolation!

#### Things

"Free time" for parents working from home with children. Catching up on movies and video games. Stress. Running marathons with bikes.

Remote learning, remote work, and video meetings. Bunnies and allergies in springtime. Skyrim! And immunotherapy and poisons.

#### Bread

Making bread while locked down! Peanut butter and jelly (jam!). Sourdough cultures. Making bread from actual ancient Egyptian yeast!
