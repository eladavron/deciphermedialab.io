---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-05-23 07:45:31+00:00
description: "Miyazaki and Nausicaa! Technological anxiety, the nature of nature and our part in it, reiterating the importance of the scientific method, Elmo's song"
layout: podcast_post
permalink: /decipherscifi/nausicaa-feat-miles-greb-comic-creator-episode-91
redirect_from:
  - /decipherscifi/91
slug: nausicaa-feat-miles-greb-comic-creator-episode-91
title: 'Nausicaa: rewilding, technological anxiety, and what is "nature?"  w/ Miles Greb'
tags:
  - animals
  - anime
  - climate change
  - ecology
  - nature
  - science
  - science fiction
  - technology

# Episode particulars
image:
  path: assets/imgs/media/movies/nausicaa_1970.jpg
  alt: Nausiccaa flying movie backdrop
links:
  - text: Espers - From Nothing, Something Comes by Miles Greb
    urls:
      - text: Kickstarter
        url: 'https://www.kickstarter.com/projects/919052445/espers-from-nothing-something-comes'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Nausica%C3%A4-Valley-Two-Disc-Blu-ray-Combo/dp/B004CRR9G0'
  title: Nausicaa of the Valley of the Wind
  type: movie
  year: 1970
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: miles_greb
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/nausicaa_1970.jpg
  number: 91
  guid: 'http://www.decipherscifi.com/?p=1540'
  media:
    audio:
      audio/mpeg:
        content_length: 40372561
        duration: '48:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Nausicaa_--_Miles_Greb_--_decipherSciFi.mp3'
---
#### Miyazaki recognition

This is Miles' favorite. Chris doesn't generally love Miyazaki but really really loves Totoro.

#### Environmentalism in Media

Processing human-caused catastrophe. We previously explored this in our [Godzilla episode]({% link decipherscifi/_posts/2016-07-19-godzilla-miles-greb-movie-podcast-episode-47.md %}) with Miles. Miyazaki was inspired by [Minamata disease](https://en.wikipedia.org/wiki/Minamata_disease).

#### Aircraft

irl Nausicaa glider!

{% youtube "https://www.youtube.com/watch?v=P5ftsTVwTfs" %}

#### Rewilding

The ways in which humanity is worse than poison. Thriving of wildlife in our absence. Predators.

{% youtube "https://www.youtube.com/watch?v=E-h15wX14po" %}

#### What is nature

Colloquial definitions versus recognition that we are part of nature and not really separate from it.

#### Technology Anxiety

Nuclear power safety and the negative effects of fossil fuels. Recognizing the relative safeness and harm. Being responsible with our technology.

#### Preservation

Of knowledge and technology over epochal time. Infrastructure and production. The scientific method as essential technology.
