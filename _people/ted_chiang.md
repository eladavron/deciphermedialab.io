---
layout: person_page
person_data:
  name: Ted Chiang
  data_name: ted_chiang
  image_path: assets/imgs/people/ted_chiang.png
  title: Sci-Fi Author
  bio: 'His work has won four Nebula awards, four Hugo awards, the John W. Campbell Award for Best New Writer, and four Locus awards. His short story "Story of Your Life" was the basis of the film Arrival (2016).'
  roles:
    - show: decipherscifi
      role: guest_cohost
---
