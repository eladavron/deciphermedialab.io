---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-05-17 07:00:23+00:00
description: 'On our first of two shows about Interstellar we talk ecological and agricultural disasters, corn, economic collapse, scientific method, awesome robots, more'
layout: podcast_post
permalink: /decipherscifi/interstellar-part-1-movie-podcast-episode-38
redirect_from:
  - /decipherscifi/38
slug: interstellar-part-1-movie-podcast-episode-38
title: 'Interstellar Part 1: monoculture farming, robot locomotion, and Kip Thorne'
tags:
  - science fiction
  - science
  - physics
  - astrophysics
  - the dust bowl
  - history
  - ecology
  - carl sagan
  - corn
  - robots

# Episode particulars
image:
  path: assets/imgs/media/movies/interstellar_2015_pt1.jpg
  alt: Interstellar family on Earth movie backdrop
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/interstellar/id960891136?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Interstellar-Matthew-McConaughey/dp/B00TU9UO1W'
  title: Interstellar
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/interstellar_2015_pt1.jpg
  number: 38
  guid: 'http://www.decipherscifi.com/?p=772'
  media:
    audio:
      audio/mpeg:
        content_length: 24370910
        duration: '50:44'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Interstellar_01_decipherSciFi.mp3'
---
#### Memories of Carl Sagan's _Contact_

Kip Thorne does movie science again. Carl Sagan's Contact. Mathew McConaughey plays two opposing roles.

#### Corn, Corn, and Corn

Monocultures. Blight. We talk about anthropogenic climate change even though it isn't mentioned in the film. Reduced population. Governments.

#### Explorers and Pioneers

Scientific mindset. Humanity's will to expand. Good science parenting.

#### Robots

TARS. 2001. Humor settings and the best robot locomotion.
