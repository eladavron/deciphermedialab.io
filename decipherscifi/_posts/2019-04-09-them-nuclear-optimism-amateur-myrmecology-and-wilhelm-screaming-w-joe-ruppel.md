---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-04-09 07:45:46+00:00
description: 'The rise of the nuclear monsters. Nuclear optimism and nuclear pessimism over time. Ant combat features. Burning down whole continents for a Simpsons joke.'
layout: podcast_post
permalink: /decipherscifi/them-nuclear-optimism-amateur-myrmecology-and-wilhelm-screaming-w-joe-ruppel
redirect_from:
  - /decipherscifi/188
slug: them-nuclear-optimism-amateur-myrmecology-and-wilhelm-screaming-w-joe-ruppel
title: 'Them!: nuclear optimism, amateur myrmecology, and Wilhelm-screaming w/ Joe Ruppel'
tags:
  - evolution
  - insects
  - mutation
  - nuclear power
  - nuclear weapons
  - nuclear winter
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/them_1954.jpg
  alt: 'Classic 1950s giant monsters eating folks'
links:
  - text: 'Wargames: computers in the 80s, weapons to end all wars, and dinosaurs w/ Joe Ruppel'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/76'
  - text: 'Superman IV: Superman hair materials science, denuclearization, and pushing the moon w/ Jolene Creighton'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/183'
  - text: 'NUKEMAP'
    urls:
      - text: 'NuclearSecrecy.com'
        url: 'https://nuclearsecrecy.com/nukemap/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/them/id293973100?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Them-James-Whitmore/dp/B001QE9WJE'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=lOTwYWW5UNE'
  title: 'Them!'
  type: movie
  year: 1954
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/them_1954.jpg
  number: 188
  guid: 'https://decipherscifi.com/?p=10358'
  media:
    audio:
      audio/mpeg:
        content_length: 35791775
        duration: '42:35'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Them_--_Joe_Ruppel_--_Decipher_SciFi.mp3'

---
#### Rise of the nuclear monsters

The era of nuclear nervousness and the nuclear monsters in cinema. "Take an object, and biggify it." Nuclear power in the 1950s. The possibility of "unmetered" energy. White Sands, gypsum, and the [Trinity test](https://en.wikipedia.org/wiki/Trinity_(nuclear_test)).

#### Nuclear optimism

The nuclear success hit list and the optimism of the early 50s. Recognizing downsides. Converting hydrogen into helium, but like really violently. Eclipses, occlusion, and we didn't realize it at the time but Joe was right to use "[occultation](https://en.wiktionary.org/wiki/occultation)." Las Vegas atomic bomb parties.

#### Nuclear fallout

The fallout from southern Nevada testing traveling directly as far as northeast Utah. The bitter, metallic taste of nuclear fallout. [When Kodak accidentally discovered a-bomb testing](https://www.popularmechanics.com/science/energy/a21382/how-kodak-accidentally-discovered-radioactive-fallout/).

#### A sense of scale

The scale of nuclear weapons since the first atom bombs. Considering the time when ["duck and cover" made sense](https://99percentinvisible.org/episode/atomic-tattoos/). Using [NUKEMAP](https://nuclearsecrecy.com/nukemap/) for a sense of scale.

#### Ants

Issues with upscaling. Myrmecology. Antennae and wayfinding. Evolutoniary history and their waspy ancestors. Ants as superorganism. Formic acid attack spray.

{% youtube "https://www.youtube.com/watch?v=f_PsPR9WtDc" %}
