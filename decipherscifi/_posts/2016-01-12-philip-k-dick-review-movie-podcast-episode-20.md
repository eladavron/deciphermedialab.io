---
# General post frontmatter
categories:
  - episode
  - interview
  - topic
date: '2016-01-12 04:45:26+00:00'
description: 'Philip K Dick Discussion. We go over PKD movies. Then we have Dan Abella, the Director of the PKD Film Festival, on to talk with us about the import of PKD.'
layout: podcast_post
permalink: /decipherscifi/philip-k-dick-review-movie-podcast-episode-20
redirect_from:
  - /decipherscifi/20
slug: philip-k-dick-review-movie-podcast-episode-20
title: 'Philip K Dick: valis, valium, and adaptations w/ PKD FIlm Festival Director Daniel Abella'
tags:
  - a scanner darkly
  - blade runner
  - philip k dick
  - philosophy
  - science
  - science fiction
  - valis
  - video games

# Episode particulars
image:
  path: assets/imgs/media/misc/philip_k_dick_header.png
  alt: 'Philip K Dick collage backdrop'
links:
  - text: The Philip K Dick Film Festival
    url: 'http://www.thephilipkdickfilmfestival.com/'
  - text: Minority Report
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/minority-report/id573718569?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/Minority-Report-Tom-Cruise/dp/B00A2FSXHK'
  - text: A collection of free PKD audiobooks and ebooks
    url: 'http://www.openculture.com/2013/03/download_14_great_sci-fi_stories_by_philip_k_dick_as_free_audio_books_and_free_ebooks.html'
  - text: The Man in the High Castle
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/The-New-World/dp/B00RSGIVVO/ref=sr_1_1?ie=UTF8&qid=1452582867&sr=8-1&keywords=man+in+the+high+castle'
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_abella
    roles:
      - guest
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 20
  guid: 'http://www.decipherscifi.com/?p=432'
  media:
    audio:
      audio/mpeg:
        content_length: 24174619
        duration: '33:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Philip_K_Dick_decipherSciFi.mp3'
---
### Philip K Dick

The importance of his work. Valis experience. Drugs! PKD's place in the pantheon of science fiction writers, among Asimov, Bradbury and others. Humanity. Dignity. Technology vs humanity.

> As we come closer to the machine, the machine comes closer to us. -Daniel Abella
>
> Daniel Abella

#### Movies Based on the Work of PKD About Which We Have Something to Say
* [Blade Runner]({% link decipherscifi/_posts/2017-08-29-blade-runner-episode-105.md %})
* Total Recall
* [A Scanner Darkly]({ post_url /decipherscifi/2015-10-20-a-scanner-darkly-podcast-episode-008 %})
* [Minority Report]({ post_url /decipherscifi/2016-11-22-minority-report-eliot-peper-author-episode-65 %})
* Paycheck
* Next
* Screamers
* The Adjustment Bureau
* Man in the High Castle

#### Philip K. Dick Film Festival

Indie sci-fi film festival. Featuring a bunch of interesting films, shorts, and documentaries. Even one video game! Here are some trailers:

#### Patreon

We've just launched our Patreon to help us improve the show! Check out the [Support the Show](https://deciphermedia.tv/supporttheshow/) page to see our spiel.
