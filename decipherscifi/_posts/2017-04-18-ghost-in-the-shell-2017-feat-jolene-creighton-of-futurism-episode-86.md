---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-04-18 06:12:17+00:00
description: 'Jolene Creighton helps us sort out the new live-action reimagining of the cyberpunk classic. Cyborgs, robot eyes, invisibility via metamaterials, humanity'
layout: podcast_post
permalink: /decipherscifi/ghost-in-the-shell-2017-feat-jolene-creighton-of-futurism-episode-86
redirect_from:
  - /decipherscifi/86
slug: ghost-in-the-shell-2017-feat-jolene-creighton-of-futurism-episode-86
title: 'Ghost in the Shell: modern cyberpunk, biorobots, and the nature of self w/ Jolene Creighton'
tags:
  - anime
  - cyborgs
  - identity
  - invisibility
  - man vs machine
  - memory
  - robots
  - science
  - science fiction
  - volumetric projection

# Episode particulars
image:
  path: assets/imgs/media/movies/ghost_in_the_shell_2017.jpg
  alt: Live-action GiTS Major jumping through glass and firing a pistol
links:
  - text: Ghost in the Shell (the real one!)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/ghost-in-the-shell/id368627419?at=1001l7hP&mt=6&uo=8&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Ghost-Shell-Richard-Epcar/dp/B000VTNLBU'
  - text: 'Ghost in the Shell: scifi anime, computer evolution, and cybernetics'
    urls:
      - text: Decipher SciFi
        url: 'decipherscifi/_posts/2015-12-08-ghost-in-the-shell-podcast-episode-15.md'
  - text: Measure of a Man - Star Trek TNG
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/measure-of-a-man/id467933759?i=467942872&mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/dp/B005HEPP0E'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/ghost-in-the-shell/id1213171514?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/GHOST-IN-THE-SHELL-Blu-ray/dp/B06XT8SXTS'
  title: Ghost in the Shell
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/ghost_in_the_shell_2017.jpg
  number: 86
  guid: 'http://www.decipherscifi.com/?p=1492'
  media:
    audio:
      audio/mpeg:
        content_length: 31961088
        duration: '38:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Ghost_in_the_Shell_2017_--_Jolene_Creighton_--_decipherSciFi.mp3'
---
#### Future City

Where is this movie taking place? Was it made explicit? We're thinking neo-Tokyo but what's really interesting is the [lengths they went to](http://www.atlasofwonders.com/2017/03/ghost-in-shell-filming-locations.html) in order to make New Zealand look like a retro-futuristic Asian city.

#### Cyberpunk

Roots in Neuromancer etc. The rising Asian power of the period (Japan) and the mixing of western and eastern aesthetics.

#### Robots!

And robot materials. Robot doctors. Metamaterials. Biorobots and cyborgs. Metal vs biomech. Blending in. Squishiness.

#### Robot eyes

Networked AR lens implants. Batou had a whole suite of EM filters! Thermal, night vision, and x-ray. X-ray! Otherwise known as "cancer mode." How x-rays work and the danger he must posing to everyone around him.

#### Invisibility Cloaks

Harry potter and Predator. First Predator vision, now predator invisisuit! Bending different frequencies of light around objects cloaked with metamaterials.

{% youtube "https://www.youtube.com/watch?v=26J5n_8_6TQ" %}

#### Memory and Self

Memory "glitch" medication. Implanting learning. Wiping memories and adapting brains to new bodies.

#### Man  vs Machine

The potential foreignness of artificial intelligence. The human brain as a meaning-making machine. Melding with the machines.

#### Nature of Self

And dualism and the great questions raised in the original film.
