---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-03-01 05:45:21+00:00
description: 'Dean Martin of LSG joins us in discussing the Wing Commander movie. The games come up, as well as space cats, pilgrim courtship, space combat, and more'
layout: podcast_post
permalink: /decipherscifi/wing-commander-movie-podcast-dean-martin-episode-27
redirect_from:
  - /decipherscifi/27
slug: wing-commander-movie-podcast-dean-martin-episode-27
title: 'Wing Commander: PC games nostalgia, pilgrims, and space combat w/ Dean from LSG Media'
tags:
  - science fiction
  - science
  - nostalgia
  - video games
  - pc gaming
  - space
  - space warfare
  - interstellar space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/wing_commander_1999.jpg
  alt: Wing Commander movie backdrop
links:
  - text: LSG Media 
    url: 'http://www.libertystreetgeek.net/'
  - text: The Wing Commander video game series
    urls:
      - text: Good Old Games
        url: 'http://www.gog.com/games?sort=bestselling&search=wing%20commander&page=1'
  - text: The Lost Fleet series by  Jack Campbell
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-lost-fleet-dauntless/id357926150?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Lost-Fleet-Dauntless-Jack-Campbell-ebook/dp/B000OZ0NXU'
  - text: Battlestar Galactica (RMG)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/bsg-the-complete-series-vol.-1/id583461291?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/33/dp/B002HGVUB8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/wing-commander/id271553048?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Wing-Commander-Freddie-Prinze-Jr/dp/B000I9X6FO'
  title: Wing Commander
  type: movie
  year: 1999
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: dean_martin
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 27
  guid: 'http://www.decipherscifi.com/?p=584'
  media:
    audio:
      audio/mpeg:
        content_length: 28635671
        duration: '47:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Wing_Commander_Dean_Martin_decipherSciFi.mp3'
---
#### Video Games

As children of the 90s, we have to wax nostalgic over the Wing Commander video games. They were really, really important at the time. Ground-breaking, even. PC games pre-windows. FMV.

  * 1990 Wing Commander
  * 1991 Wing Commander 2
  * 1994 Wing Commander 3
    - 5 million bux
  * 1996/7 Wing Commander 4
    - 10 million bux
  * 1999 Wing Commander Movie
    - 30 million bux

{% youtube "https://www.youtube.com/watch?v=7qYItb4g6qg" %}

#### Chris Roberts

Director of the games was also director of the film! We hypothesize based on zero knowledge of anything how it may have been difficult for this first-time feature film director. We consider whether he is the George Lucas of video games, in both the good or bad senses.

#### Kilrathi

Cat creatures? This may not have been clear in the film. Hairless cats.

> Mister colbert, mount the forward laser pointer!

{% youtube "https://www.youtube.com/watch?v=wfAoVX_VWWs" %}

#### Space Combat and Naval/Air Combat

Simplifications of space combat, ignoring the vast distances and incredible speeds in their portrayals. Things that did it pretty right.

#### A Crew of Mavericks

At least in Top Gun, there was only one Maverick. Here we have to consider if we'd want a ship full of Mavericks as Earth's last best hope.

#### Space Romance

One makes sense. One seems to come out of nowhere. Maniac is awful and dangerous. This movie's idea of "search and rescue" seems a bit _off_.

#### Pilgrims and Navigation

Pilgrim discrimination. Pilgrim backstory. Using pilgrim brains in the Navcom. Faith vs genetics.
