---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-10-31 07:45:41+00:00
description: 'The Fly! Teleporter (non)science, genie computer AI, sorta subverting mad scientist tropes, DNA analysis and coherent combination, the edges of "you," more!'
layout: podcast_post
permalink: /decipherscifi/the-fly-1986-episode-114
redirect_from:
  - /decipherscifi/114
slug: the-fly-1986-episode-114
title: 'The Fly: teleporters, dna detection, and arthropod digestion'
tags:
  - ai
  - body horror
  - dna
  - flies
  - genetic engineering
  - insects
  - mad scientist
  - science
  - science fiction
  - teleportation

# Episode particulars
image:
  path: assets/imgs/media/movies/the_fly_1986.jpg
  alt: Sexy naked Goldblum in a transporter pod thing
links:
  - text: Flies lifecycle
    urls:
      - text: YouTube
        url: 'https://youtu.be/lcjAedlIPoU'
  - text: A Scientist Responds… to The Fly
    urls:
      - text: io9
        url: 'https://io9.gizmodo.com/a-scientist-responds-to-the-fly-1720875696'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-fly-1986/id396199412?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Fly-Jeff-Goldblum/dp/B004DHUB28'
  title: The Fly
  type: movie
  year: 1986
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_fly_1986.jpg
  number: 114
  guid: 'https://decipherscifi.com/?p=3923'
  media:
    audio:
      audio/mpeg:
        content_length: 31066112
        duration: '36:58'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Fly_--_--_decipherSciFi.mp3'
---
#### Body horror

> Body horror, biological horror, organic horror or visceral horror is horror fiction in which the horror is principally derived from the unnatural graphic transformation, degeneration or destruction of the physical body.
>
> -[Wikipedia](https://en.wikipedia.org/wiki/List_of_body_horror_media)

Cronenberg! And Carpenter! You may recall some similar horror themes from our episodes on [Event Horizon]({% link decipherscifi/_posts/2017-10-24-event-horizon-feat-caelum-rale-episode-113.md %}) (though that concentrated rather on the cosmic horror) and [The Thing]({% link decipherscifi/_posts/2016-10-25-carpenters-the-thing-feat-cae-ryan-episode-61.md %}).

#### Mad... scientist?

More like mad "project manager," as he describes himself. How refreshing to see something sidestep the trope of the [omnicapable super-scientist](http://tvtropes.org/pmwiki/pmwiki.php/Main/OmnidisciplinaryScientist) and have someone NOT create world-changing supertech by themselves. The art of the kludge/bodge.

{% youtube "https://www.youtube.com/watch?v=lIFE7h3m40U" %}

#### The computer

It's basically a genie! Rules-lawyering a computer is programming a computer. The nature of this _interpretive_ computer system and the feats of artificial intelligence that enable it to coherently fuse Brundle and fly together.

#### Teleporter

Materials-detection, DNA analysis, and black-box DNA recombination algorithms. That's a hell of a design. And the voice recognition isn't too shabby, either.

#### DNA detection

How do we tell between species by DNA these days? [DNA barcoding](https://en.wikipedia.org/wiki/DNA_barcoding)! Find a good, differentiable locus and make a database of that area for a certain type of earth life. Where do we keep the database? The [Barcode of Life Data System](http://www.boldsystems.org/).

#### How genetic fusion teleportation could go wrong

Animate vs inanimate. DNA fusion or DNA damage and how and when problems could manifest. Expressing or ignoring large chunks of odd DNA. The unlikelihood of coherent expression of fly-like traits from the fused fly DNA. Junk DNA and its influence on our robustness against fly fusion.

#### Turning into a fly

_Gross_ hairs. Super strength and the [square cube law](https://en.wikipedia.org/wiki/Square%E2%80%93cube_law) a la our [King Kong]({% link decipherscifi/_posts/2017-07-25-kong-skull-island-episode-100.md %}) episode. Enzymatic digestion. Insect reproductive cycles.
