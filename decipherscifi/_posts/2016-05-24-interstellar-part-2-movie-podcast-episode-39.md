---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-05-24 05:18:37+00:00
description: 'In our second episode on Interstellar, we get to the meat: physics! Black holes, wormholes, time dilation, dimensional abstractions, Carl Sagan, more'
layout: podcast_post
permalink: /decipherscifi/interstellar-part-2-movie-podcast-episode-39
redirect_from:
  - /decipherscifi/39
slug: interstellar-part-2-movie-podcast-episode-39
title: 'Interstellar Part 2: wormholes, time dilation, and love stuff'
tags:
  - science fiction
  - science
  - black holes
  - physics
  - astrophysics
  - universe
  - human evolution
  - stars
  - dimensionality
  - love
  - space settlement
  - time dilation

# Episode particulars
image:
  path: assets/imgs/media/movies/interstellar_2015_pt2.jpg
  alt: Interstellar Matthew McConaughey in a spacesuit on an icy planet movie backdrop
links:
  - text: Flatland & The Fourth Dimension by Carl Sagan
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=iiWKq57uAlk'
  - text: The Science of Interstellar w/ Kip Thorne
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=lM-N0tbwBB4'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/interstellar/id960891136?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Interstellar-Matthew-McConaughey/dp/B00TU9UO1W'
  title: Interstellar
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/interstellar_2015_pt2.jpg
  number: 39
  guid: 'http://www.decipherscifi.com/?p=787'
  media:
    audio:
      audio/mpeg:
        content_length: 30446208
        duration: '50:44'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Interstellar_02_decipherSciFi.mp3'
---
#### Gravity and Black Holes

More Kip Thorne. Wormholes. Detecting gravitational anomalies.

#### Time Dilation

Einstein. Train rides. Relativity. Frames of reference. Frame dragging. Time as an expendable resource.

#### Crazy Planetary Adventures

Water planet is the stuff of Christopher's nightmares.  Frozen clouds!

#### So Many Dimensions

Abstraction of extra dimensions into fewer. Tesseracts. Carl Sagan's genius powers of explanation.

#### Time Travel Paradoxes

Predestination paradox? Self-consistent single timelines.

#### Love

Multidimensional beings and the power of love. Social utility and bonding, etc.

#### Space Settlement

O'Niell cylinders. Another call-in from Liam Ginty of [Voices From L5](https://www.patreon.com/VoicesFromL5/posts).
