---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-05-30 09:45:30+00:00
description: 'Midnight Special was really good. Inter- and extra-dimensionality, creatures from them, quantum decryption, the "beings of pure energy" trope, more'
layout: podcast_post
permalink: /decipherscifi/midnight-special-episode-92
redirect_from:
  - /decipherscifi/92
slug: midnight-special-episode-92
title: 'Midnight Special: fatherhood, synesthesia, and unconventional computing'
tags:
  - aliens
  - dimensions
  - radio
  - science
  - science fiction
  - superpowers
  - synesthesia

# Episode particulars
image:
  path: assets/imgs/media/movies/midnight_special_2016.jpg
  alt: 
links:
  - text: Carl Sagan on Dimensions
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=N0WjV6MmCyM'
  - text: 'Arrival Part 1: superdimensionality and alien physics w/ Jolene & Adrian'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/64'
  - text:' Interstellar Part 2: wormholes, time dilation, and love stuff'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/39'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/midnight-special-2016/id1089418156?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Midnight-Special-Michael-Shannon/dp/B01EXR2R06'
  title: Midnight Special
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/midnight_special_2016.jpg
  number: 92
  guid: 'http://www.decipherscifi.com/?p=1551'
  media:
    audio:
      audio/mpeg:
        content_length: 31630513
        duration: '37:38'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Midnight_Special_--_--_decipherSciFi.mp3'
---
#### Superpowers?

Energy usage and biclops lasereyes.

#### Radio reception

The old wive's tale of dental fillings picking up terrestrial radio signals. Satellite military communications frequencies.

#### Alternative Computing

Quantum and DNA computing. Synesthesia. Recognizing that we are all synesthetes within the bounds of "normal" human brain operations.

{% youtube "https://www.youtube.com/watch?v=rkRbebvoYqI" %}

#### Dimensions

Extra- super- ultra-dimensionality. Flatland _again_.

#### Superdimensional Beings

"Beings of light" and why this might be the most reasonable example of the trope. The world on the other side.
