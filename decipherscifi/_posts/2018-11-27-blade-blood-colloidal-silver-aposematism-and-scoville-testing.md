---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - guest co-host
  - movie
  - tv
date: 2018-11-27 07:45:17+00:00
description: 'Vampires! Blood. Blood disorders. Weaponizing silver. How to blue oneself. Scoville unit science. Written language decipherment. Manuscript restoration.'
layout: podcast_post
permalink: /decipherscifi/blade-blood-colloidal-silver-aposematism-and-scoville-testing
redirect_from:
  - /decipherscifi/169
slug: blade-blood-colloidal-silver-aposematism-and-scoville-testing
title: 'Blade: blood, colloidal silver aposematism, and scoville testing'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/blade_1998.jpg
  alt: 
links:
  - text: 'The World in UV by Veritassium'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=V9K6gjR07Po'
  - text: 'Sunscreen in UV by Physics Girl'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=GRD-xvlhGMc'
  - text: 'Red Bull Crashed Ice 2012 Uphill Race'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Qw81qpYeaZA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/blade/id292720888?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Blade-Wesley-Snipes/dp/B001RJTFP4/'
  title: Blade
  type: movie
  year: 1998
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/blade_1998.jpg
  number: 169
  guid: 'https://decipherscifi.com/?p=8317'
  media:
    audio:
      audio/mpeg:
        content_length: 30290684
        duration: '36:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Blade_--_--_Decipher_SciFi.mp3'
---
#### Blade

Enthusiasm!

#### Silver

Weaponizing silver. Colloidal silver. Papa Smurf. Blue anti-vampire [aposematism](https://en.wikipedia.org/wiki/Aposematism). [Agyria](https://en.wikipedia.org/wiki/Argyria#Society_and_culture) a la Paul Karason aka Papa smurf.

#### Spray defense

Pepper spray. Garlic spray. The difference between bear spray and self-defense pepper spray. The positives and negatives of fog-mode sprays in vampire club fighting. Johnny Knoxville pain ratings.

#### Scoville units

Laboratory capsaicin measurement by high-performance liquid chromatography vs the organoleptic method. The obvious unreliability of the original organoleptic (tasting) method.

#### Blood

Blooooood. Blood drinking. Hemoglobin deficiencies. Heme. [Porphyria](https://en.wikipedia.org/wiki/Porphyria) and the vampire legend. Blood doping.

#### Sun block

Vampires and ultraviolet sensitivity. "Chemical" and "physical" UV sunblocks.

#### Decipherment

Decipherment vs translation. Ancient manuscripts. [Multispectral imaging](https://www.youtube.com/watch?v=WHjKnNbYiOs). Machine learning supremacy. Making random noise until you know how to play Mario.
