FROM ubuntu:latest

# Update everything
RUN apt-get update\
    && apt-get upgrade -y\
    && apt-get dist-upgrade -y\
    && apt-get autoremove -y

ENV DEBIAN_FRONTEND=noninteractive

# Install Ruby
RUN apt-get install -y vim nano ruby ruby-dev git gcc make g++ pkg-config libmagickcore-dev libmagickwand-dev\
    && echo 'export GEM_HOME="${HOME}/.gems"' >> "${HOME}/.bash_profile"\
    && echo 'export PATH="${HOME}/.gems/bin:${PATH}"' >> "${HOME}/.bash_profile"\
    && gem install bundler

# Copy the source code to the docker
COPY . /src
WORKDIR /src
RUN mkdir .bundle && echo -e '---\nBUNDLE_PATH: "vendor"' > .bundle/config

# Build it!
RUN bundle install\
    && bundle exec jekyll build

EXPOSE 4000
CMD ["bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0"]