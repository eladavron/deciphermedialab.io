---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - guest co-host
  - movie
  - tv
  - conversation
date: 
description: 
layout: podcast_post
redirect_from:
  - /decipherhistory/#
slug: 
title: 
tags:
  - history

# Episode particulars
image:
  path: assets/imgs/media/movies/
  alt: 
links:
  - text: 
    urls:
      - text: 
        url: ''
  - text: 
    urls:
      - text: 
        url: ''
  - text: 
    urls:
      - text: 
        url: ''
media:
  links:
    - text: iTunes
      url: ''
    - text: Amazon
      url: ''
  title: 
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: SOMEGUEST
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/
  number: 
  guid: 
  media:
    audio:
      audio/mpeg:
        content_length: 
        duration: ''
        explicit: false
        url: ''

---
