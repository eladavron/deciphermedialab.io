---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-05-26T00:28:50-04:00
description: 'Deep sea engineering! Surviving at depth. Atmospheric mixtures. Liquid pistol shrimp cryptids, exothermic explosions, and long-range radio. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/239
slug: underwater-deep-drilling-nitrogen-narcosis-helium-voice
title: 'Underwater: deep drilling, nitrogen narcosis, and astronaut helium voice'
tags:
  - engineering
  - monster
  - ocean science
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/underwater_2020.jpg
  alt: 'Kristen Stewart in a space marine sea suit'
links:
  - text: "Astronaut Scott Carpenter Speaks to President Johnson from a Helium-Atmosphere Decompression Chamber"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Gg0pMbc7Opk'
  - text: "Mom's Spaghetti by Eminem"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=SW-BU6keEUw'
  - text: 'Underwater Alternate Ending'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=fM9NUoQEUkc'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/underwater/id1492777767?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Underwater-Kristen-Stewart/dp/B0854L3LW7'
  title: Underwater
  type: movie
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/underwater_2020.jpg
  number: 239
  guid: underwater-deep-drilling-nitrogen-narcosis-helium-voice
  media:
    audio:
      audio/mpeg:
        content_length: 34811843
        duration: '41:23'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/underwater.final.mp3'

---
#### Deep water drilling

It's expensive! And if oil is ever expensive enough to justify the cost of drilling in Challenger Deep then by golly you start investing in alternative power instead. Semi-submersible deep-water drilling platforms. Drillships. We drill kinda deep in the ocean but like, not *actually* deep. Record drill depths and the vast gulf from there to the Mariana Trench.

#### The ocean: scarier than space

Gravity meets Aliens. Pressures and temperatures that are more deadly than getting spaced out of an airlock. The deepest hole humans have ever drilled was barely deeper than Challenger Deep is under the surface of the ocean.

#### Living deep undersea

Nitrogen narcosis: it's bad for you! Atmospheric mixture issues under pressure. [Sealab I, II, and III](https://en.wikipedia.org/wiki/SEALAB). Aquanatical research. [Sperm whales get the bends](https://www.nature.com/articles/news041220-13). Oh no your suit [some kind of exploded](https://www.youtube.com/channel/UCcMDMoNu66_1Hwi5-MeiQgw). Monsters of the deep. [Deep sea gigantism](https://en.wikipedia.org/wiki/Deep-sea_gigantism).


{% youtube "https://www.youtube.com/watch?v=EJiUWBiM8HE" %}

<br>

{% youtube "https://www.youtube.com/watch?v=gx7V6c_2Jyo" %}

#### Construction

Engineering against 16,000 psi and freezing temperatures. Concrete strengths and saltwater issues.

#### Misc

Exothermic implosions - they're like explosions, but backwards! Pistol shrimp. *Long*-range radio communications through water. [ELF](https://en.wikipedia.org/wiki/Extremely_low_frequency). [Past discussions]({% link decipherscifi/_posts/2018-08-28-rampage-gorilla-sign-language-classic-gaming-and-the-square-cube-law.md %}) on the matter. Dumb infrared.
