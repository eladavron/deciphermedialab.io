---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-09-04 07:45:04+00:00
description: 'Doggo appreciation, evolution, and domestication. Watermelons, too! How we bend nature to our use. IRL animal biomechanics for your robot. Flamethrowers.'
layout: podcast_post
permalink: /decipherscifi/a-x-l-dogs-as-technology-domestication-and-unpleasant-sticky-materials
redirect_from:
  - /decipherscifi/158
slug: a-x-l-dogs-as-technology-domestication-and-unpleasant-sticky-materials
title: 'A-X-L: dogs as technology, domestication, and unpleasant sticky materials'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/axl_2018.jpg
  alt: 'A boy and his robo-dog. AXL movie backdrop.'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/a-x-l/id1433630316?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/X-L-Becky-G/dp/B07KY48L3G'
  title: 'A.X.L.'
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/axl_2018.jpg
  number: 158
  guid: 'https://decipherscifi.com/?p=7587'
  media:
    audio:
      audio/mpeg:
        content_length: 25064932
        duration: '29:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/AXL_--_--_Decipher_SciFi.mp3'
---
#### Doggo

Man's best friend! And one of (many of?) our best technologies. [The level to which we can handicap animals for aesthetics if we put our mind to it](https://sploid.gizmodo.com/how-a-century-of-dog-breeding-ruined-these-beautiful-an-1477122149). Domesticated foxes.

#### Domestication

Domestication. Plants _and_ animals. Bending chosen qualities to better human use.

[Watermelon as of The Renaissance](https://www.vox.com/2015/7/28/9050469/watermelon-breeding-paintings). Yay selective breeding! We win!

#### Warbot design

Implicit and explicit communication channels. Dog human body language-reading. Data connections and cloudy signal processing. Adaptation of dog body-design to robots. The small design effort required to get people to treat a robot animal like an animal.

#### Flamethrowers

The difference between industrial and military flamethrowers. An unpleasant, sticky material: napalm and napalm-like products!

#### Survey!

We have one of those. Pls help by hitting the link we gave in the episode!
