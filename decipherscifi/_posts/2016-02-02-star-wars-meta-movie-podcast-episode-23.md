---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2016-02-02 05:45:59+00:00'
description: 'Our Star Wars meta discussion covers merchandising revolutions, VFX, viewing order (machete!), Star Wars Revisited, Star Wars Despecialized Edition, more.'
layout: podcast_post
permalink: /decipherscifi/star-wars-meta-movie-podcast-episode-23
redirect_from:
  - /decipherscifi/23
slug: star-wars-meta-movie-podcast-episode-23
title: 'Star Wars Meta: How to Star Wars, machete order, and the best Star Wars fan edit'
tags:
  - fandom
  - machete order
  - science
  - science fiction
  - star wars

# Episode particulars
image:
  path: assets/imgs/media/movies/star_wars_meta.jpg
  alt: Star War meta backdrop
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie-collection/star-wars-digital-movie-collection/id982709307?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Star-Wars-Digital-Movie-Collection/dp/B00VJ04TH0'
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: /assets/imgs/decipherscifi/itunes-2018-07-10.tn.jpg
  number: 23
  guid: 'http://www.decipherscifi.com/?p=491'
  media:
    audio:
      audio/mpeg:
        content_length: 19855378
        duration: '33:05'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Star_Wars_Meta_decipherSciFi.mp3'
---
Our Star Wars meta discussion covers merchandising revolutions, VFX, viewing order (machete!), Star Wars Revisited, Star Wars Despecialized Edition, more.

#### VFX

George Lucas. Industrial Light and Magic. Practical effects vs. CG.

#### Merchandising/Marketing

Star Wars was a revolution in movie merchandising.
{% youtube  "https://www.youtube.com/watch?v=uxKwaG1idBU" %}

#### Official Release Editions

Original Trilogy only because the prequels are less complicated and we just kinda don't care

* 1985 VHS, pretty much theatrical
* 1993 Definitive Collection (The Laserdisc). Grognards dig this one. For some reason.
* 1997 Special Edition - the one that ruined Star Wars according to nerds. Reportedly cost like $10 million to do this work and it just made everyone mad. Lots of CGI was overlaid atop non-CG stuff from the original. New scenes, different music. Introduced the Greedo shooting first thing, which is stupid and paints the character of Han incorrectly.
* 2004 Original Trilogy DVDs - More changes. Nerds say the color correction is garbage, sound was messed up.
* 2006 Star Wars Trilogy Limited Edition Box Set. Had the 2004 editions of each film, but ALSO the original “theatrical cuts”. Actually the same cut as the laserdisc. They are letterboxed and grainy and bad with lots of video quality issues and look like shit.
* 2011 Star Wars Complete Saga Bluray - Basically the same thing as 2004 DVDs, but on Bluray.

#### Viewing Order

It's important! [Here](http://www.nomachetejuggling.com/2011/11/11/the-star-wars-saga-suggested-viewing-order/) is the original (really good!) article for machete order, but if you need the tldr it's 4,5,2,3, and 6.

#### The Phantom Edit

Google for it, maybe try to find a copy on Youtube. It's around. It's an edit of the prequel trilogy which puts them all into one feature-length movie, mostly leaving out the first film.

#### Fan Edits

* [Star Wars Revisited](https://swrevisited.wordpress.com/anhr-change-list/) - LOTS of changes. Christopher is partial to this one. Starts with theatrical and makes it beautiful. Color correction improved. More ships and more destruction in space battles, makes it feel more epic. Also presumes a bit much and makes some questionable changes, but these can be avoided by getting the “purist” edition. So not only did they do this, but they separated into different editions depending on how sensitive you are to different issues. Holy crap, so much effort.

{% youtube "https://www.youtube.com/watch?v=H-URD9L1Vxo" %}

* [Despecialized edition](https://www.facebook.com/despecialized/info/?tab=page_info) - _THIS_ is the one to watch if you have no idea. Based on 2011 Bluray, with bits from various other cuts where needed. Removed all special edition changes, pretty much. Made it look great. There are special high quality theatrical prints around used as color reference. Here is a video outlining the changes in general.

{% youtube "https://www.youtube.com/watch?v=QXifjbxZDAM" %}

* Others - so many others! But you shouldn’t care, mostly. The other two are the important ones to know about.
