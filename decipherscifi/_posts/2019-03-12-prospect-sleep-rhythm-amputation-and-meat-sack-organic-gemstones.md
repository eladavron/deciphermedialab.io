---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-03-12 07:45:03+00:00
description: 'Impressive indie scifi. Blindness, retinal sensitivity, and sleep rhythms. Alien meat testicle gems. Stuffing things into wounds. Efficient amputation. Etc.'
layout: podcast_post
permalink: /decipherscifi/prospect-sleep-rhythm-amputation-and-meat-sack-organic-gemstones
redirect_from:
  - /decipherscifi/184
slug: prospect-sleep-rhythm-amputation-and-meat-sack-organic-gemstones
title: 'Prospect: sleep rhythm, amputation, and meat-sack organic gemstones'
tags:
  - biology
  - evolution
  - medicine
  - science
  - science fiction
  - sleep
  - space exploration
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/prospect_2018.jpg
  alt: 'A man and a girl in some janky spacesuits'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/prospect/id1438703753?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Prospect-Sophie-Thatcher/dp/B07P9BJLK6'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=m7i2Qu2vFvk'
  title: Prospect
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/prospect_2018.jpg
  number: 184
  guid: 'https://decipherscifi.com/?p=10320'
  media:
    audio:
      audio/mpeg:
        content_length: 27694847
        duration: '32:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Prospect_--_--_Decipher_SciFi.mp3'

---
#### Indie scifi

Impressive shoestring productions. Remembering [_Moon_]({% link decipherscifi/_posts/2015-09-15-moon-movie-podcast-episode-003.md %}). Lived-in duct-tape science fiction.

#### Sleep cycles

Circadian rhythms. Setting our clocks by light exposure. Light exposure and activity levels. Wearing your sunglasses at night. Different varieties of blindness and sleep rhythms. Retinal sensitivity to light.

#### Space prospectin'

Alien meat-testicle gems. Personal testicular security systems. Organic gemstones. Pearl production. How attractive our kidney stones might be to alien speculators. "Real" utility vs perceived value.

#### Wound handling

Wound sealing. Stopping bleeding in an emergency with caulking foam. Packing a wound full of tiny tampons. Failed experiments in would sealing with: beads, marbles, beans, etc. Amputation history. Ancient Greek amputation practices and efficiency records in the Napoleonic and American Civil Wars.
