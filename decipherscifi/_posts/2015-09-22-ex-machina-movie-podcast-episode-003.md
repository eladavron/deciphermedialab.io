---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-09-22 03:45:16+00:00'
description: 'With Ex Machina we talk science fiction and lone genius inventors, Turing test, the personification of AI, uncanny valley, AI dangers, machine learning, AI ethics'
layout: podcast_post
permalink: /decipherscifi/ex-machina-movie-podcast-episode-003
redirect_from:
  - /decipherscifi/4
slug: ex-machina-movie-podcast-episode-003
title: 'Ex Machina: ai software, wetware, and the turing test'
tags:
  - artificial intelligence
  - cyborg
  - ethics
  - future
  - lone genius
  - man versus machine
  - robot
  - science fiction
  - turing test

# Episode particulars
image:
  path: assets/imgs/media/movies/ex_machina_2015.jpg
  alt: 'Ex Machina backdrop'
links:
  - text: 'Superintelligence: Paths, Dangers, Strategies - by Nick Bostrom'
    urls:
      -  text: iTunes
         url: 'https://geo.itunes.apple.com/us/book/superintelligence-paths-dangers/id899568056?mt=11&at=1001l7hP'
      -  text: Amazon
         url: 'http://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom-ebook/dp/B00LOOCGB2'
  - text: 'Our Final Invention - by James Barrat'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/our-final-invention/id647685260?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Our-Final-Invention-Artificial-Intelligence-ebook/dp/B00CQYAWRY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/ex-machina/id983488795?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'http://www.amazon.com/Ex-Machina-Alicia-Vikander/dp/B011KKCQH8'
  title: 'Ex Machina'
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/ex_machina_2015.jpg
  number: 4
  guid: 'http://www.decipherscifi.com/?p=216'
  media:
    audio:
      audio/mpeg:
        content_length: 28191855
        duration: '46:58'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Ex_Machina_decipherSciFi.mp3'

---
#### Lone genius inventor

The myth. Thomas Edison. Google. Boss guy taking credit. Standing on the shoulders of giants. Could the open source movement make great breakthroughs possible for small groups? Didn't it already?

#### Turing Test

Commonly misunderstood. But the intelligence in this movie is way past that anyway. Testing for theory of mind might be the right track for the future.

#### Personification of AI

Being a cute, vulnerable girl AI goes a long way. Uncanny valley. Sexual attraction to non-human humanoid intelligences.

#### AI Danger

Fast/slow takeoff. AI will look back on us like pre-Sapiens fossils. It is not difficult to "escape the box." If a person can do it, so can a superintelligence.

#### Machine Learning/Data Collection

Google might be the biggest and most successful AI project in the world. Phone spying. Black-box neural networks. Algorithmic complexity.

#### AI Ethics

Is it immoral to create an AI and not let it be free? "Do you have someone testing you who could turn you off?" Romantic interest better than morals? Does Nathan consider how he is treating the AIs? God complex. Baghavad Gita. The end of humanity. AI arms race.

[spoiler title='Spoiler Notes' style='default' collapse_link='true']Section Did Ava leave Caleb to die? Did she do it purposely? What will her next move be?[/spoiler]
