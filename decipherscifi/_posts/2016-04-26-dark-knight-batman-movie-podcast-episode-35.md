---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-04-26 09:00:35+00:00
description: 'With Dark Knight we talk Batman and his enemies, whether Batman is sci-fi, gadgets, sonar phone hacking, order and fairness and chaos, more'
layout: podcast_post
permalink: /decipherscifi/dark-knight-batman-movie-podcast-episode-35
redirect_from:
  - /decipherscifi/35
slug: dark-knight-batman-movie-podcast-episode-35
title: 'The Dark Knight: Batman gadgets, sonar phones, and blowing up the boat'
tags:
  - science fiction
  - science
  - batman
  - comics
  - comic books
  - superheroes

# Episode particulars
image:
  path: assets/imgs/media/movies/the_dark_knight_2008.jpg
  alt: The Dark Knight Batman movie backdrop
links:
  - text: Batman Chooses His Voice by Pete Holmes
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=WOg3ZE3hNQc'
  - text: Batman The Animated Series
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/batman-animated-series-vol./id282974093?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Christmas-With-The-Joker/dp/B001BXQ97I'
  - text: Heart of Ice from Batman TAS
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=7EtHfr0eBA4'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-dark-knight/id764632601?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Dark-Knight-Christian-Bale/dp/B001OGWY1W'
  title: The Dark Knight
  type: movie
  year: 2008
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_dark_knight_2008.jpg
  number: 35
  guid: 'http://www.decipherscifi.com/?p=731'
  media:
    audio:
      audio/mpeg:
        content_length: 21740123
        duration: '36:13'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Dark_Knight_Batman_decipherSciFi.mp3'
---
#### Is Batman science fiction?

Short answer: yeah, sure. Featuring a voice clip from [Liam Ginty, space nerd]({% link _people/liam_ginty.md %}).

#### Batman's detective work

CSI bullet reconstructions. The trappings of science.

#### Batman's Bondian Gadgetry

All kinds. The skyhook.

{% youtube "https://www.youtube.com/watch?v=PErEsNhDmo8" %}

#### Sonar Phones

Prerequisite technologies. Zero day vulnerabilities and phone hacking. Distributed computing to combine the massive data. Google's Project Tango.

{% youtube "https://www.youtube.com/watch?v=tPR9EFE20Aw" %}

#### Law, Fairness, and Chaos

Batman and his enemies in the film.
