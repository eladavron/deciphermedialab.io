---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-11-17 04:45:47+00:00'
description: 'With The Martian on the podcast we talk clever movie marketing, the book, scientific accuracy, real travel to Mars, water on Mars, settling Mars, MacGyver astronauts.'
layout: podcast_post
permalink: /decipherscifi/the-martian-podcast-episode-12
redirect_from:
  - /decipherscifi/12
slug: the-martian-podcast-episode-12
title: 'The Martian: scientific accuracy, mars colonies, and poop potatoes'
tags:
  - science fiction
  - mars
  - space settlement
  - nasa
  - space ship
  - survival
  - space x
  - elon musk
  - water on mars
  - robots

# Episode particulars
image:
  path: assets/imgs/media/movies/the_martian_2015.jpg
  alt: The Martian Matt Damon backdrop
links:
  - text: Digital Dads - Exploring the Final Frontier
    urls:
      - text: Part 1
        url: 'http://digitaldads.fm/2015/03/31/episode-15-exploring-the-final-frontier-part-1/'
      - text: Part 2
        url: 'http://digitaldads.fm/2015/03/31/episode-16-exploring-the-final-frontier-part-2/'
  - text: The Case For Mars - by Robert Zubrin
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/case-for-mars/id448231990?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Case-Mars-Robert-Zubrin-ebook/dp/B004G8QU6U'
  - text: Spin - by Robert Charles Wilson
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/spin/id418293539?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Spin-Robert-Charles-Wilson-ebook/dp/B0016IXMWI'
media:
  links:
    - text: iTunes
      url: 'https://traffic.libsyn.com/decipherscifi/The_Martian_decipherSciFi.mp3'
    - text: Amazon
      url: 'https://www.amazon.com/Martian-Michael-Pe%C3%B1a/dp/B018HIZ65U'
  title: The Martian
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_martian_2015.jpg
  number: 12
  guid: 'http://www.decipherscifi.com/?p=342'
  media:
    audio:
      audio/mpeg:
        content_length: 35139533
        duration: '48:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Martian_decipherSciFi.mp3'

---
#### The Film's Marketing

Really cool [Youtube channel](https://www.youtube.com/channel/UCAHwvVPQZggKTgQlVF88rGw). Neil deGrasse Tyson. Spoiler policies.
{% youtube "https://www.youtube.com/watch?v=-fdKyszL1Zo" %}

#### The Book

Serially published on Andy Weir's blog. Accidental best-seller.

#### Nailed the Science

Research. Space travel is dangerous. Radiation shielding.

#### IRL Travel to Mars

Mars One. Space X. ELON MUSK. Settlement.

#### Water on Mars

Liquid salinated water. Frozen water under surface. NASA sterilization.

#### Worth it?

Robots? Human inspiration.

#### Settling/Colonizing Mars

Growing or shipping food.

#### Resourcefulness of Astronauts

Pretty much have to be MacGyver. Overcoming functional fixedness.
