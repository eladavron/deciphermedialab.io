---
layout: person_page
person_data:
  name: Daniel Mann
  data_name: daniel_mann
  image_path: assets/imgs/people/daniel_mann.png
  title: Software Pro
  bio: 'Dan is a fancypants programmer consultant and has been recognized with the Microsoft MVP award multiple times. He’s also incredibly hairy – like a gorilla.'
  roles:
    - show: decipherscifi
      role: guest_cohost
---
