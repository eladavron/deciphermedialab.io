---
layout: page
title: About Decipher Media
---

Decipher Media is a [listener-supported] producer of fun and informative indie podcasts about science, history, and general geekery.

We are [Christopher Peterson] and [Lee Colbert], and with the help of a bunch of our friends (some of whom you [may recognize](/people/)!) we use popular media to connect ideas and learn something surprising with you every week.

{% responsive_image path: assets/imgs/site/us.jpg template: _includes/image_inline.html alt: "Decipher Media Hosts cartoon portrait" %}

# Free and Open

[DecipherMedia.tv site source code repo](https://gitlab.com/deciphermedia/deciphermedia.gitlab.io)

This whole site and its code are free/open source (except where otherwise specified). So you can learn from it, reuse it, or even make changes to it yourself.

Our shows (which are distributed via the open RSS standard) are [licensed] under [Creative Commons Attribution-NonCommercial International 4.0](https://creativecommons.org/licenses/by-nc/4.0/), so you can use our media in your own productions or even redistribute it noncommercially. You just need to always credit us, link back to our website, and do not publish the site or a feeds in a way that could compete with our own. Anything else, just ask! 🙂

The media and tools produced by others that we in turn use to make this site and our shows, you'll find listed on our [attribution] page.

# Privacy

We value digital freedom (_yours_ in particular!), so we don't do any invasive tracking on this website. <3

But our pages aren't the only concern here! There is also our audio media host, as well as the webhost serving these very pages.

Our media host Libsyn has a [privacy policy](https://libsyn.com/tos-policies/privacy-policy/) as well as a [GDPR FAQ](https://libsyn.com/tos-policies/gdpr-faq/). This applies to any media downloads i.e. the podcast media files themselves, when played either in the browser or via your favorite podcatcher.

Our website host, Gitlab Pages, has [their own privacy policy](https://about.gitlab.com/privacy/) as well as a [page regarding GDPR compliance](https://about.gitlab.com/gdpr/) This applies as to anyone visiting this website since, while we don't do any tracking of our own, your communication with a webserver provides information like your IP address which is considered "personal data" under laws like [GDPR](https://en.wikipedia.org/wiki/GDPR).

[attribution]: /attribution/
[Christopher Peterson]: /people/christopher_peterson/
[licensed]: /attribution/
[Lee Colbert]: /people/lee_colbert/
[listener-supported]: /supporttheshow
