---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-10-03 07:45:16+00:00
description: "Pitch Black science! Stasis, micrometeorites, landing on mystery planets, trinary star systems, Riddick's eye-shine, glow worms, cicada raptors, more"
layout: podcast_post
permalink: /decipherscifi/pitch-black-feat-fraser-cain-of-universe-today-episode-110
redirect_from:
  - /decipherscifi/110
slug: pitch-black-feat-fraser-cain-of-universe-today-episode-110
title: 'Pitch Black: tri star systems, micrometeorites, and eyeshine w/ Fraser Cain'
tags:
  - bioluminescence
  - evolution
  - impactors
  - science
  - science fiction
  - space
  - stars
  - stasis

# Episode particulars
image:
  path: assets/imgs/media/movies/pitch_black_2000.jpg
  alt: Riddick in the dark with his arms crossed menacingly
links:
  - text: Fraser Cain's different stuff
    urls:
      - text: Astronomy Cast
        url: 'http://www.astronomycast.com/'
      - text: Universe Today
        url: 'https://www.universetoday.com/'
      - text: YouTube
        url: 'https://www.youtube.com/user/universetoday'
  - text: 'Farewell Cassini: The Grand Finale and the Final Images of Saturn by Fraser Cain'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=aqSCoQJxbMI'
  - text: What If Saturn Paid Us a Very Close Visit? by Phil Plait
    urls:
      - text: Bad Astronomy
        url: 'http://www.slate.com/blogs/bad_astronomy/2014/05/12/close_encounter_what_if_saturn_swung_by_earth.html'
  - text: The Three-Body Problem by Cixin Liu
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-three-body-problem/id856893409?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Three-Body-Problem-Remembrance-Earths-Past-ebook/dp/B00IQO403K'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/pitch-black-unrated/id280387469?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Pitch-Black-Unrated-Directors-Cut/dp/B001BRAUYM'
  title: Pitch Black
  type: movie
  year: 2000
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/pitch_black_2000.jpg
  number: 110
  guid: 'https://decipherscifi.com/?p=3525'
  media:
    audio:
      audio/mpeg:
        content_length: 51589120
        duration: '01:01:24'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Pitch_Black_--_Fraser_Cain_--_decipherSciFi.mp3'
---
#### Stasis

Stasis for interstellar space travel. "Freezing" people into a low-activity biological state. IRL medical developments.

#### Microimpactors in space

Space junk in near-earth orbit. That time a paint chipped [cracked a window on the ISS](https://www.popsci.com/paint-chip-likely-caused-window-damage-on-space-station) (don't worry, the ISS is fine). Comet tails and variously-sized chunks of ice as impactors.

#### Landing on M6-117 (and other planets)

Atmospheric density vs interstellar velocities. The benefits of landing on a planet _with_ atmosphere vs a planet without. It's hard without! Realizing having an atmosphere to slow down in can be a blessing if the ship can handle the temperatures due to atmospheric compression under/around the vessel.

#### Trinary Star systems

Trinary systems and the chance for stable planetary orbits. Proxima Centauri. Globular star clusters as alternative for "all light all the time."

#### Bioraptors (the monsters)

Predators on the "cicada cycle." How the ecology of this world appears or does not appear to support this cycle. Slowly-heating stars and the shifting "goldilocks" zone.

#### Riddick

Eyeshine! Light amplification or translation from outside of our cone of EM reception. Night vision. Replacing eyeballs with robust cameras. [Ghost in the Shell and X-ray vision]({% link decipherscifi/_posts/2017-04-18-ghost-in-the-shell-2017-feat-jolene-creighton-of-futurism-episode-86.md %}).

#### Glow-worms

Evolving bioluminescence as a food-collection mechanism vs keeping away the flying death-machines.
