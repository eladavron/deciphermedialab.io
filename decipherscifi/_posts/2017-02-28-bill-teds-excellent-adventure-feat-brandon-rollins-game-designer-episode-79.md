---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-02-28 07:48:52+00:00 
description: "Napoleon wasn't short! How Beethoven wrote music while mostly and then completely deaf. The mismatch between person and myth. 15th-century English. More" 
layout: podcast_post
permalink: /decipherscifi/bill-teds-excellent-adventure-feat-brandon-rollins-game-designer-episode-79
redirect_from:
  - /decipherscifi/79
slug: bill-teds-excellent-adventure-feat-brandon-rollins-game-designer-episode-79 
title: "Bill & Ted's Excellent Adventure: history vs myth, and time travel  w/ Brandon Rollins" 
tags:
  - science fiction
  - science
  - future
  - history
  - time travel
  - utopia
  - time loops

# Episode particulars
image:
  path: assets/imgs/media/movies/bill_and_teds_excellent_adventure_1989.jpg
  alt: Bill and Ted sitting atop a flying phonebooth time machine
links:
  - text: War Co Expandable Game
    urls:
      - text: WarCoTheGame
        url: 'http://warcothegame.com/'
  - text: Wrath of the Khans by Dan Carlin
    urls:
      - text: Hardcore History
        url: 'http://www.dancarlin.com/product/hardcore-history-wrath-of-the-khans-series/'
  - text: Doctor Who Explains the Bootstrap Paradox
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=u4SEDzynMiQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/bill-teds-excellent-adventure/id309099454?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'http://www.amazon.com/Bill-Excellent-Adventure-Keanu-Reeves/dp/B002BRGEMS'
  title: "Bill & Ted's Excellent Adventure"
  type: movie
  year: 79
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: brandon_rollins
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/bill_and_teds_excellent_adventure_1989.jpg
  number: 79
  guid: 'http://www.decipherscifi.com/?p=1384'
  media:
    audio:
      audio/mpeg:
        content_length: 37089280
        duration: '44:09'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Bill_and_Teds_Excellent_Adventure_--_Brandon_Rollins_--_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/bill_and_teds_excellent_adventure.jpg alt: "Decipher SciFi hosts and Brandon Rollins cosplaying as Bill & Ted" %}

#### Mismatching of Person and Myth

Flawed characters in history. Remembering to recognize that the characters we imagine may not be as close as we think to the real thing.

#### Napoleon

A short dead dude, but also irl an imposing figure. And not actually short! Of totally average height - the shortness thing was actually an [ex post facto error ](https://en.wikipedia.org/wiki/Napoleon#Image)in conversion from the units of measurement of his time and place.

#### Socrates

Socrates -> Plato -> Aristotle. The Hemlock treatment vs living in the future. Socratic ignorance: "the only true wisdom is knowing that you know nothing." Bill and Ted's rolled high on wisdom, low on intelligence. :)

#### Beethoven

Gradual deafness. Bill & Ted picked him up in 1810, four years before he was reported to have gone completely deaf. So he may have had some limited ability to hear. Piano on the floor, stylus in the mouth, and other coping mechanisms. [Beethoven's metronome](http://www.radiolab.org/story/269783-speedy-beet/). OR MAYBE, he was actually just a crazy old smelly deaf guy and that's why mall security had to come drag him away.

#### King Henry VII

They visit his castle in 15th century England and meet two totally bodacious babes. Or something. Attention to linguistic detail in the film: everybody speaks the correct languages! More or less. 15th century English. Chaucerean and Shakespearean pronunciation.

{% youtube "https://www.youtube.com/watch?v=B5QAV6lOCnQ" %}

{% youtube "https://www.youtube.com/watch?v=qYiYd9RcK5M" %}

#### The Princess Situation

Can they read? Write? Reading and writing as two separate skills. Credit cards. Introducing medieval princesses to fiat currency and a debt-based economy.

#### Genghis Khan

Dan Carlin said all of it better than we ever could: [Wrath of the Khans](http://www.dancarlin.com/product/hardcore-history-wrath-of-the-khans-series/).

#### Time Travel

[Causal loops](https://en.wikipedia.org/wiki/Time_loop). Bootstrap and [grandfather](https://en.wikipedia.org/wiki/Grandfather_paradox) paradoxes. [Dr Who](https://www.youtube.com/watch?v=u4SEDzynMiQ) on the bootstrap paradox. "Time circuits."
