---
layout: person_page
person_data:
  name: Jonathan Korman
  data_name: jonathan_korman
  image_path: assets/imgs/people/jonathan_korman.png
  title: User experience strategist
  bio: 'Writes, talks, thinks, teaches, and learns about philosophy, comparative religion, magic, artificial intelligence, human physical and mental augmentation, pop culture, and how they all relate.'
  websites:
    - name: Miniver Cheevy
      url: 'http://miniver.blogspot.com/'
  social:
    - name: twitter
      url: 'https://twitter.com/miniver'
  roles:
    - show: decipherscifi
      role: roundtable_guest

---
