---
layout: person_page
person_data:
  name: Zoltan Istvan
  data_name: zoltan_istvan
  image_path: assets/imgs/people/zoltan_istvan.png
  title: Transhumanist, Politician
  bio: 'American transhumanist, journalist, entrepreneur, and Libertarian futurist -Wikipedia'
  websites:
    - name: Zoltan Istvan
      url: 'http://www.zoltanistvan.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/zoltan_istvan
    - name: instagram
      url: https://www.instagram.com/zoltan_istvan
    - facebook: zoltangistvan
  roles:
    - show: decipherscifi
      role: guest_cohost
---
