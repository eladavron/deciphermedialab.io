---
# General post frontmatter
author: christopher_peterson
categories:
  - irl
date: 2017-10-05 17:21:09+00:00 
layout: post
slug: blade-runner-2049-nyc-meetup 
title: 'Upcoming: Blade Runner 2049 NYC Meetup'
image:
  path: assets/imgs/media/movies/blade_runner_2049_2017.jpg
  alt: 'Blade Runner 2049 movie backdrop'

---

NYC meetup to see Blade Runner 2049!

[Here](https://www.facebook.com/events/1424310137687800/) is the Facebook event page if you wanna join in. We'll get together for the movie and then see if everyone has time to hang afterwards. And here are the details sans-Facebook:


**Blade Runner 2049**
**7:30 PM**
[**AMC Loews 34th Street 14**](https://www.google.com/maps/place/AMC+Loews+34th+Street+14/@40.7523536,-73.9966789,17z/data=!3m1!4b1!4m5!3m4!1s0x89c259adf0f84c77:0x77dca1d4dfedcadf!8m2!3d40.7523496!4d-73.9944849)
**[Tickets](https://www.amctheatres.com/showtimes/blade-runner-2049-50377/2017-10-06/amc-loews-34th-street-14/all/59952022)**


You might prepare yourself by re-listening to our first [Blade Runner]({% link decipherscifi/_posts/2017-08-29-blade-runner-episode-105.md %}) episode. Then [subscribe]({{ site.url }}/decipherscifi) and keep an eye out for the new one!
