---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-08-06 07:45:04+00:00
description: 'How to best smuggle weapons into the past? The meatball method. Time travel paradoxes. 1980s nostalgia. Machine consciousness. Arnold! Cameron!'
layout: podcast_post
permalink: /decipherscifi/the-terminator-bootstrapping-machine-uprising-and-time-traveling-meatballs
redirect_from:
  - /decipherscifi/205
slug: the-terminator-bootstrapping-machine-uprising-and-time-traveling-meatballs
title: 'The Terminator: bootstrapping, machine uprising, and time traveling meatballs w/ Joe Ruppel'
tags:
  - arnold schwarzenegger
  - cyborgs
  - information
  - robots
  - science
  - science fiction
  - terminator
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/the_terminator_1984.jpg
  alt: Arnold being the original intimidating bad-guy Terminator
links:
  - text: 'Harlan Ellison and the Terminator'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=RRXB0h7sf70'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-terminator/id271991087?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Terminator-Arnold-Schwarzenegger/dp/B00153ZC8Q'
  title: The Terminator
  type: movie
  year: 1984
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_terminator_1984.jpg
  number: 205
  guid: 'https://decipherscifi.com/?p=10724'
  media:
    audio:
      audio/mpeg:
        content_length: 36020387
        duration: '42:46'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/the_terminator_1984.mp3'

---
#### People

James Cameron's first film! And Arnold is amazing! The possibility of casting O.J. Simpson as the T-100. Harlan Ellison lawsuit.

#### Robots

The thawing of the AI winter of the 70s. Robots finally showing up in our lives and in the workplace. Terminator bootstrapping. Robotics and machine learning.

#### Skynet

The possibility of AI becoming "self-aware" or "conscious" and whether we even know what those things really mean. Big data feeding skynet.

#### Terminators

Jacked-up and oiled beefcake "infiltration" models. Franco Columbu. Robotic Ahhhhhnold blending in. The "rule of cool." Feeding and maintaining a terminator's "living tissue."

#### Persistence of information

The persistence of human knowledge after the nuclear apocalypse. The fragility of magnetic storage media. Storing your data under a mountain. The weakness of encryption over time.

#### Time travel

How to not sound crazy when telling people in the past that you're from *the future*. Meatball time travel. Using the "living tissue" time travel rule to transport large weapons into the past. Stuffing a whale or elephant. Realizing that maybe (probably), _no one_ (not even SkyNet) has the slightest clue what they are doing with the time travel.
