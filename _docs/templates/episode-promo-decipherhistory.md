---
# General post frontmatter
categories:
  - episode
  - bonus
date: 
layout: podcast_post
slug: on-decipher-history-
title: 'On Decipher History: '

# Episode particulars
image:
  path: 
  alt: 
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: DATA_NAME
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  media:
    audio:
      audio/mpeg:
        content_length: 
        duration: ''
        explicit: false
        url: ''

---
The taste of another episode of Decipher History! Featuring 

[](https://deciphermedia.tv/decipherhistory/)

