---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2016-01-26 05:45:39+00:00'
description: 'With Contact on the podcast we talk Carl Sagan, Carl Sagan, Carl Sagan, and Carl Sagan. Also SETI, aliens, deep opponent characterizations, atheism.'
layout: podcast_post
permalink: /decipherscifi/contact-we-love-carl-sagan-episode-22
redirect_from:
  - /decipherscifi/22
slug: contact-we-love-carl-sagan-episode-22
title: 'Contact: SETI, Carl Sagan love, and scicomm'
tags:
  - atheism
  - carl sagan
  - faith
  - religion
  - science
  - science communication
  - science fiction
  - scientist
  - space
  - space travel
  - universe

# Episode particulars
image:
  path: assets/imgs/media/movies/contact_1997.jpg
  alt: Contact movie backdrop
links:
  - text: Contact  - by Carl Sagan
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/contact/id1183142078?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Cosmos-Carl-Sagan-ebook/dp/B004W0HZN4'
  - text: Cosmos (1980)
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Pc1NatQKSP0&list=PLxlNGNNSIm8o94YV7wNNfVxJKvBjz7ydt'
  - text: Cosmos (2015)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/cosmos-spacetime-odyssey-season/id888781083?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Standing-Up-Milky-Way-HD/dp/B00IJL1J02'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/contact/id279598767?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Contact-Jodie-Foster/dp/B001AH6ZWY'
  title: Contact
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 22
  guid: 'http://www.decipherscifi.com/?p=497'
  media:
    audio:
      audio/mpeg:
        content_length: 27805738
        duration: '46:20'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Contact_decipherSciFi.mp3'
---
#### Carl Sagan

Our love for Carl. How his character and interests manifest in the characters in the film.

#### SETI

What it is. Why it is. Why is doesn't get taken seriously. [Their website](http://www.seti.org/).

#### Real Science

The story of Carl's question for Kip Thorn producing actual time travel science. Black holes and wormholes.

#### Carl's Model Scientist

The Jodie Foster character really emulates Carl in a lot of ways. Also partially based on SETI astronomer Jill Tarter.

#### Role Models/Parents

The dad in the movie. Carl Sagan's gentle and understanding skepticism.

#### Aliens

How to communicate with aliens. Dimensions and moves of communication.

#### Atheism/Agnosticism

No one trusts atheists. Our feelings on the Jodie Foster character and our own positions on things.
