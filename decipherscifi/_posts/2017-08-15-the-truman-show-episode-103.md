---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-08-15 07:45:51+00:00
description: 'Truman Show science! Domed cities on Earth and beyond. Seeing manmade objects from space. IRL Truman Show delusion, reality testing best practices, more'
layout: podcast_post
permalink: /decipherscifi/the-truman-show-episode-103
redirect_from:
  - /decipherscifi/103
slug: the-truman-show-episode-103
title: 'The Truman Show: objects visible from space, dome megastructues, and reality testing'
tags:
  - delusions
  - domes
  - reality testing
  - reality tv
  - science
  - science fiction
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/the_truman_show_1998.jpg
  alt: Truman as a mosaic of candid camera shots, Truman show movie backdrop
links:
  - text: How Much of the Earth Can You See at Once? by Vsauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=mxhxL1LzKww'
  - text: The Truman Delusion/Being Famous byh Johnny Benjamin
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=D5XdrG5mf5Y'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-truman-show/id311726400?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Truman-Show-Jim-Carrey/dp/B002SGYPS2'
  title: The Truman Show
  type: movie
  year: 1998
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_truman_show_1998.jpg
  number: 103
  guid: 'https://decipherscifi.com/?p=2980'
  media:
    audio:
      audio/mpeg:
        content_length: 27254784
        duration: '32:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Truman_Show_--_--_decipherSciFi.mp3'
---
#### Reality TV

The Real World, reality TV, etc... and then The Truman Show. Thank the writers strikes and the economics of production sans writers.

#### Seeing things from space

Calculating the size of the Truman show dome from the satellite images in the film. Seeing man-made things from space. How [the Great Wall is _not_ visible from space](https://www.universetoday.com/25364/can-you-see-the-great-wall-of-china-from-space/) but [the pyramids are](https://www.universetoday.com/93398/can-you-see-the-pyramids-from-space/). And where does "space" begin, anyway? Looking from the ISS.

#### The Dome!

Weather systems inside man-made structures. The dome is plenty large enough to have its own weather systems. Hugeness and economic realities. Other [scifi domed-city discussions]({% link decipherscifi/_posts/2016-09-13-cowboy-bebop-movie-podcast-episode-55.md %}).

#### Product Placement/Advertising

And our own plans for how to sell out. Thoughts on "Gorilla" marketing.
Unrelated: send advertising inquiries to contact@deciphermedia.tv (｡◝‿◜｡)

#### The Truman show Delusion

Colbert's mustache woes. Delusions of grandeur over time. [This guy's really interesting personal story](https://www.youtube.com/watch?v=D5XdrG5mf5Y) of his experience with "the Truman Show Delusion."

#### Reality Testing

How would _you_ know if it were you? Occam's razor. Glitches in the matrix. Plato's Cave.
