---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-02-13 07:45:20+00:00
description: 'Transformers science, Michael Baysplosions, historical warfare and trebuchets. King Arthur and legends vs folktales vs myth. The hugeness of the cosmos, ghost scrotum, and subverting the trope of the useful scientist, more'
layout: podcast_post
permalink: /decipherscifi/transformers-the-last-knight-feat-jolene-creighton-from-futurism-episode-129
redirect_from:
  - /decipherscifi/129
slug: transformers-the-last-knight-feat-jolene-creighton-from-futurism-episode-129
title: 'Transformers The Last Knight: the size of the cosmos, colliding planets, and the useful scientist w/ Jolene Creighton'
tags:
  - earth-like planets
  - medieval war technology
  - myth
  - oribts
  - paper
  - planets
  - science
  - science fiction
  - scientists

# Episode particulars
image:
  path: assets/imgs/media/movies/transformers_the_last_knight_2017.jpg
  alt: Giant robots fighting Transformers The Last Knight backdrop
links:
  - text: Futurism
    urls:
      - text: Futurism.com
        url: 'http://futurism.com/'
  - text: What is Bayhem? by Every Frame a Painting
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=2THVvshvq0Q'
  - text: 'When Worlds Collide: observable universe, 20th century astonomy, and modern space industry w/ Daniel James Barker'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/118'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/transformers-the-last-knight-digital/id1242544331?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Transformers-Last-Knight-Mark-Wahlberg/dp/B07215NWRL'
  title: Transformers The Last Knight
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/transformers_the_last_knight_2017.jpg
  number: 129
  guid: 'https://decipherscifi.com/?p=5129'
  media:
    audio:
      audio/mpeg:
        content_length: 35487744
        duration: '42:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Transformers_The_Last_Knight_--_Jolene_Creighton_--_decipherSciFi.mp3'
---
{% responsive_image path: assets/imgs/selfies/trasnformers_the_last_knight.jpg alt: "Decipher SciFi hosts with robo-Jolene" %}

#### Michael Bay

{% youtube "https://www.youtube.com/watch?v=v7ssUivM-eM" %}

#### The spark of life

Unicron and the spark of life between worlds.

#### War in The Dark Ages

Trebuchets. Traction trebuchet vs counterweight trebuchet. Siege weapons vs anti-personnel. Army sizes in the early middle ages.

#### King Arthur

Legend vs myth vs folk tales. Staff-bearer selection methodology. Humans all look the same to Transformers.

> The classic definition of myth, legend, and folktale is William Bascom's 1965 article "The Form of Folklore: Prose Narratives." Bascom defines folktales as prose narratives told as fiction, myths as prose narratives told as true set in the remote past before human history, and legends as prose narratives told as true set in the time of human history. (Bascom does not take up fables, but they are generally considered a type of folktale with animal characters and an obvious moral lesson.) However, folklorists today tend to focus less on the matter of form and cross-cultural classification---any one of these genres can be conveyed in non-prose, non-narrative media---and more on how people classify, communicate, and contextualize their own folk narratives. A sacred text conceived as the true and literal account of creation to one person may be considered an entertaining but completely fantastical account by another, even if the form remains exactly the same. Bascom's attention to belief in classifying folk narrative remains an important contribution, and folklorists today consider how tellers and audiences of folk narrative negotiate belief and skepticism in the narrative event, offering proofs that the events happened as described ("I didn't use to believe in ghosts, but I saw it with my own eyes") or leaving room for some doubt.
>
> Jolene's friend Steve Stanzak who is a folklorist from Indiana University...

#### The size and nature of the cosmos

The number of planets in the solar system and the percentage that are "Earth-like" and might harbor life. Our expanding understanding of the richness of the cosmos.

#### When Worlds Collide

Speaking of which, I'm starting to feel like maybe we've [covered]({% link decipherscifi/_posts/2017-02-21-independence-day-resurgence-episode-78.md %}).[this]({% link decipherscifi/_posts/2017-10-03-pitch-black-feat-fraser-cain-of-universe-today-episode-110.md %}).[before]({% link decipherscifi/_posts/2017-11-28-when-worlds-collide-feat-daniel-barker-science-communicator-episode-118.md %}). Gravity, orbit perturbations, and impending doom after the credits.

#### Ghost scrotum

Papryus, paper, and vellum. Goat scrotum!

#### Buster the Science Guy

Subverting the trope of useful scientist.
