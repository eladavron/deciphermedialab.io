---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-03-15 05:23:50+00:00
description: 'Joe Ruppel returns to share the history of Frankenstein. Ethics of mad science. Trendsetting with the book and the film. Creating cliches. Head transplants.'
layout: podcast_post
permalink: /decipherscifi/frankenstein-joe-ruppel-movie-podcast-episode-29
redirect_from:
  - /decipherscifi/29
slug: frankenstein-joe-ruppel-movie-podcast-episode-29
title: 'Frankenstein: science fiction horror, reanimation, and victorian context w/ Joe Ruppel'
tags:
  - science fiction
  - science
  - frankenstein
  - monster
  - classic horror
  - horror
  - mad scientist
  - history

# Episode particulars
image:
  path: assets/imgs/media/movies/frankenstein_1931.jpg
  alt: Frankenstein 1931 Boris Karloff movie backdrop
links:
  - text: Frankenstein by Mary Shelley
    urls:
      - text: 'Project Gutenberg (ebook)'
        url: 'http://www.gutenberg.org/ebooks/84'
      - text: 'Project Gutenberg (audibook)'
        url: 'http://www.gutenberg.org/ebooks/20038'
  - text: Young Frankenstein
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/Young-Frankenstein-Gene-Wilder/dp/B000G6BLWE'
  - text: 'The "Slip n Slide" scene from Mary Shelley''s Frankenstein'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Nm4jeqvA_JQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/frankenstein-1931/id332166136?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Frankenstein-James-Whale/dp/B002RXZF6C'
  title: Frankenstein
  type: movie
  year: 1931
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 29
  guid: 'http://www.decipherscifi.com/?p=652'
  media:
    audio:
      audio/mpeg:
        content_length: 28511833
        duration: '47:30'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Frankenstein_Joe_Ruppel.mp3'
---
#### Mary Shelley and The Book

Volcanoes and the year with no summer. Women writing books. Philosophers and children of philosophers.

#### Trendsetting

The movie vs the book. The beginning of scifi. The ways that each broke boundaries in their own ways.

#### Scary

Was it scary at the time? The movie intro. Everything that's cliché now was new once.

#### Playing God

Science, evil science, and playing god. The ethics of mad science.

#### Science Horror

How to take the cutting edge and make it a horror story. Electricity. X-rays. Quantum stuff. Nano bio tech now? Deepak Chopra is full of crap.
