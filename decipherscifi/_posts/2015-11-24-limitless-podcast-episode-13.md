---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-11-24 04:45:03+00:00'
description: 'With Limitless on the podcast we talk nootropics, brain drugs, judging intelligence, addiction, and going into politics'
layout: podcast_post
permalink: /decipherscifi/limitless-podcast-episode-13
redirect_from:
  - /decipherscifi/13
slug: limitless-podcast-episode-13
title: 'Limitless: nootropics, brain myths, and types of intelligence'
tags:
  - science fiction
  - science
  - nootropics
  - intelligence

# Episode particulars
image:
  path: assets/imgs/media/movies/limitless_2011.jpg
  alt: Limitless Bradley Cooper movie back drop
  links:
  - text: Stories of Your Life and Others - by Ted Chiang
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/stories-of-your-life/id480657666?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Stories-Your-Life-Ted-Chiang-ebook/dp/B0048EKOP0'
  - text: Flower for Algernon - Daniel Keyes
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/flowers-for-algernon/id427541168?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Flowers-for-Algernon/dp/B002NRMNR6'
  - text: Limitless (TV Series)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/limitless-season-1/id1031572562?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Pilot/dp/B014CRR2TI'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/limitless/id435942860?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Limitless-Bradley-Cooper/dp/B005DD7H50'
  title: Limitless
  type: movie
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 13
  guid: 'http://www.decipherscifi.com/?p=349'
  media:
    audio:
      audio/mpeg:
        content_length: 30457482
        duration: '42:17'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Limitless_decipherSciFi.mp3'
---
#### FX

How they produced the "zoom" sequences in the film. Color grading.

#### Brain Drugs

Drugs as tools. 20% brain myth. Christopher's past caffeine issues. Drugs have side effects, like blacking out, maybe murdering folks.

#### Intelligence

How do we define intelligence? Social? Knowledge? Pattern recognition? Analysis? Synthesis? Crazy neuro physical adaptation.

#### Thought Experiments

Would you want the drug? How could this affect society? Going into politics?
