---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-12-24 17:00:13+00:00
description: 'Adaptations. How smart are dolphins, or how are dolphins smart? Betelgeuse and dying stars gnoshing on their planet. Universal translators. AI. Big space.'
layout: podcast_post
permalink: /decipherscifi/hitchhikers-guide-to-the-galaxy-dolphin-communication-the-babelfish-and-bad-ai
redirect_from:
  - /decipherscifi/173
slug: hitchhikers-guide-to-the-galaxy-dolphin-communication-the-babelfish-and-bad-ai
title: "Hitchhiker's Guide to the Galaxy: Dolphin communication, the babelfish, and bad AI"
tags:
  - animal intelligence
  - artificial intelligence
  - science
  - science fiction
  - space
  - universal translation

# Episode particulars
image:
  path: assets/imgs/media/movies/the_hitchikers_guide_to_the_galaxy_2005.jpg
  alt: Arthur Dent and friends assembled in movie-poster fashion.
links:
  - text: "The Hitchhiker's Guide to the Galaxy The Game"
    urls:
      - text: BBC Radio 4
        url: 'http://www.bbc.co.uk/programmes/articles/1g84m0sXpnNCv84GpN2PLZG/the-game-30th-anniversary-edition'
  - text: 'DRONE Solar System Model by Mark Rober'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=pR5VJo5ifdE'
  - text: 'VFX Artist Reveals the True Scale of the Universe by Corridor Digital'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=GCTuirkcRwo'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-hitchhikers-guide-to-the-galaxy/id188702833?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Hitchhikers-Guide-Galaxy-Sam-Rockwell/dp/B003QSJVGW'
  title: "The Hitchhiker's Guide to the Galaxy"
  type: movie
  year: 2005
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_hitchikers_guide_to_the_galaxy_2005.jpg
  number: 173
  guid: 'https://decipherscifi.com/?p=9607'
  media:
    audio:
      audio/mpeg:
        content_length: 33904325
        duration: '40:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Hitchhikers_Guide_to_the_Galaxy_--_--_Decipher_SciFi.mp3'
---
#### Adaptations

Appreciating the book(s). _Bigtime_. Adapting Douglas Adams. Remembering the [text adventure video game](http://www.bbc.co.uk/programmes/articles/1g84m0sXpnNCv84GpN2PLZG/the-game-30th-anniversary-edition) exists.

#### Dolphins

Dolphin intelligence. It's not about how smart they are, it's about how they're smart. Mammalian sameness as an avenue into better understanding life in the sea. Dolphin communication. Dolphin "language" and the possibility of translation. The Dolphin [C.H.A.T.](http://www.wilddolphinproject.org/chat-is-it-a-dolphin-translator-or-an-interface/) interface.

#### Betelgeuse

Big star! Red supergiant, 9th brightest in our sky. "Brightness" outside the visible spectrum. Dying stars gnoshing on their planets before going off.

#### Life form dominance

Metal boxes, taking over the planet and slowly digesting the soft squishy things inside them. Street Fighter-style [car-breaking](https://www.youtube.com/watch?v=nk-g6ax-ByI). How to judge the "top" species of a world.

#### Babel fish

Universal translators. A visit from [Nick Farmer]({% link _people/nick_farmer.md %}) from the past, when we asked him about universal translators in our [Q&A #2]({% link decipherscifi/_posts/2018-01-09-nothing-but-gifs-qa-1-episode-124.md %}).

#### Space is big

Mind-bogglingly big! The bigness of space and the difficulty in finding a sense of scale from a human perspective.

> Space is big. You just won't believe how vastly, hugely, mind-bogglingly big it is. I mean, you may think it's a long way down the road to the chemist's, but that's just peanuts to space.
> 
> -Douglas Adams

#### AI

Doors deriving sexual pleasure from providing their service. Polar opposite extreme AI demeanors. _Deep Thought_ and AI constraints.
