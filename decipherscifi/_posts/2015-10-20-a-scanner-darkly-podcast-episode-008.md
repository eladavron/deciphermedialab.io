---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-10-20 04:45:08+00:00'
description: "With A Scanner Darkly on the podcast we talk Linklater's rotoscoping, Edward Snowden, drug addiction, drug culture, big brother, corporate conspiracies, fractured identity, more."
layout: podcast_post
permalink: /decipherscifi/a-scanner-darkly-podcast-episode-008
redirect_from:
  - /decipherscifi/8
slug: a-scanner-darkly-podcast-episode-008
title: 'A Scanner Darkly: the surveillance state, drug abuse, and rotoscoping'
tags:
  - animation
  - drugs
  - dystopia
  - future
  - identity
  - mental illness
  - philip k dick
  - rotoscoping
  - science fiction
  - surveillance

# Episode particulars
image:
  path: assets/imgs/media/movies/a_scanner_darkly_2006.jpg
  alt: 'A Scanner Darkly backdrop'
links:
  - text: a Scanner Darkly - by Philip K Dick
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/a-scanner-darkly/id427697269?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Scanner-Darkly-Philip-K-Dick-ebook/dp/B005LVR6NC'
  - text: Citizenfour
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/citizenfour/id931009631?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Citizenfour-Edward-Snowden/dp/B00VO8D13K'
  - text: Waking Life
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/waking-life/id547156351?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Waking-Life-Wiggins/dp/B008GM1G70'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/a-scanner-darkly/id293377124?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Scanner-Darkly-Keanu-Reeves/dp/B000MS6NV0'
  title: A Scanner Darkly
  type: movie
  year: 2006
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 8
  guid: 'http://www.decipherscifi.com/?p=293'
  media:
    audio:
      audio/mpeg:
        content_length: 32631399
        duration: '45:19'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/A_Scanner_Darkly_decipherSciFi.mp3'
---
#### The Novel

Philip K. Dick sort of wrote this about himself and his experiences with the Berkeley drug culture. "Everything in A Scanner Darkly I, actually saw." This novel is probably the least scifi of Dick's work.

#### Rotoscroping

Other famous examples of rotoscroping, but Linklater's are the first to do it 100%. Interesting custom software with layering techniques. Perfect for this film with its less than firm grip on reality.

#### Drug Addiction

We are squares and we know roughly nothing about drugs. But it sure seems bad. Especially the drug in the movie: Substance D. LSD experiments like the following:
[youtube]n4Sb8jCJUTw[/youtube]
[ Here](http://www.openculture.com/2013/10/artist-draws-nine-portraits-on-lsd-during-1950s-research-experiment.html) is the article about these experiments.

#### Big Brother/Corporate Conspiracy

Small conspiracies vs grand conspiracies. Big brother. Alex Jones and Linklater. Govt agencies compelling sharing by private firms. The social effects of being watched. The war on drugs.

#### Fractured Indentity

Scramble suits. How much can trust our own senses? Occam's razor. Unwitting sacrifice.
