---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-08-29 05:12:47+00:00
description: 'Blade Runner science! Testing for androids. Looking for nipples. How to watch Blade Runner. Genetic brain design. Memory, mortality, and tears in rain. More'
layout: podcast_post
permalink: /decipherscifi/blade-runner-episode-105
redirect_from:
  - /decipherscifi/105
slug: blade-runner-episode-105
title: 'Blade Runner: "androids," the AI control problem, and brain design'
tags:
  - ai
  - androids
  - cyborgs
  - memory
  - mortality
  - robots
  - science
  - science fiction
  - vfx

# Episode particulars
image:
  path: assets/imgs/media/movies/blade_runner_1982.jpg
  alt: Blade Runner cyberpunk city movie backdrop
links:
  - text: Bad Voiceover Montage from the Theatrical Release of Blade Runner
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=5A3JkjScUKY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/blade-runner-the-final-cut/id566651076?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Blade-Runner-Final-Harrison-Ford/dp/B0012PDVUS'
  title: Blade Runner
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/blade_runner_1982.jpg
  number: 105
  guid: 'https://decipherscifi.com/?p=3113'
  media:
    audio:
      audio/mpeg:
        content_length: 45899776
        duration: '54:37'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Blade_Runner_--_--_decipherSciFi.mp3'
---
#### How to watch Blade Runner

About the different releases and how very bad voiceovers can be. But tl;dr: watch the "Final Cut" for sure always.

#### Creators' Intent

[Philip K Dick]({% link decipherscifi/_posts/2016-01-12-philip-k-dick-review-movie-podcast-episode-20.md %})'s vision in the novel _Do Android Dream of Electric Sheep_ vs Ridley Scott's in the film. Dick's use of scifi as a tool to explore humanity.

#### Cyberpunk

High tech, low life. Blade Runner and William Gibson's Neuromancer. Many [other]({% link decipherscifi/_posts/2017-07-18-judge-dredd-1995-episode-99.md %}) [examples]({% link decipherscifi/_posts/2017-07-18-judge-dredd-1995-episode-99.md %}) [in]({% link decipherscifi/_posts/2016-04-19-fifth-element-movie-podcast-episode-34.md %}) [our]({% link decipherscifi/_posts/2016-01-12-philip-k-dick-review-movie-podcast-episode-20.md %}) [episode]({% link decipherscifi/_posts/2015-12-08-ghost-in-the-shell-podcast-episode-15.md %}) [history]({% link decipherscifi/_posts/2017-04-18-ghost-in-the-shell-2017-feat-jolene-creighton-of-futurism-episode-86.md %}). Social stratification and the wealth gap. Cyborgy bits and noodle bars. Environmental destruction. Flying cars?

#### VFX

And cinematography. What a beautiful movie! Practical effects and things that age well from the 80s.

#### Replicants

Detection. Genetic design. Testing for replicants with bullets like testing a witch with drowning.

#### Robotic Anthropomorphicization

Why design your androids to be so human? Designing anthropomorphized androids to operate with human tools in a human environment.

#### Robot Revelations

A new theory for how replicants work. Terminological issues. Maybe they shouldn't be called "androids?"

#### AI Issues

The control problem and cellular kill switches.

#### The Voight-Kampff Machine

The Voight-Kampff machine as analogy to the lie detector, i.e. the BS intimidation machine for coaxing people/androids into admitting things. Or shooting the interviewer. [Turtle vs tortoise vs terrapin](http://www.thesuperfins.com/how-many-species-of-turtles-exist-today/).

{% youtube "https://youtu.be/rN7pkFNEg5c" %}

#### Brain design and selection

"Android" brain production analogy with CPU production methods.

#### Memory, Mortality

Memories providing the context that makes up our _selves_. Looking for nipples.

#### Deckard's humanity

Is Deckard a replicant? It depends on whether you're talking about the book and then also which cut of the film.In the book, no. In the theatrical release, probably not. In The Final Cut, yes.
