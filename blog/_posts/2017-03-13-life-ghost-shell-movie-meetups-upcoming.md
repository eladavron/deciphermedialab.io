---
# General post frontmatter
author: christopher_peterson
categories:
  - irl
date: 2017-03-13 16:40:25+00:00 
layout: post
slug: life-ghost-shell-movie-meetups-upcoming 
title: 'Life and Ghost in the Shell Movie Meetups Upcoming' 
image:
  path: assets/imgs/misc/meetup-life-gits.jpg
  alt: 'Decipher SciFi Life and GitS movie meetup announcement image'

---
We've got two meetups upcoming!

First, we're going to see [Life](http://www.imdb.com/title/tt5442430/) at home-base in Princeton, NJ. March 25, 2017! Check out the [Facebook event page](https://www.facebook.com/events/1255607321142490/), where details will fill in as they are made available.

Then we're going back to NYC to see the live-action [Ghost in the Shell](http://www.imdb.com/title/tt1219827/). April 8, 2017! Here is the [Facebook event page](https://www.facebook.com/events/1348754191837619/), details forthcoming.

Hope to see you there!
