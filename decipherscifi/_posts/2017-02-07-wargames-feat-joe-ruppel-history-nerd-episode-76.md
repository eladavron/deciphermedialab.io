---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-02-07 09:05:19+00:00 
description: 'Joe Ruppel is back again to help explore WarGames: the 80s, the Cold War, mutually-assured destruction, machine learning, 80s computing hardware, cracking' 
layout: podcast_post
permalink: /decipherscifi/wargames-feat-joe-ruppel-history-nerd-episode-76
redirect_from:
  - /decipherscifi/76
slug: wargames-feat-joe-ruppel-history-nerd-episode-76 
title: 'Wargames: computers in the 80s, weapons to end all wars, and dinosaurs w/ Joe Ruppel' 
tags:
  - science fiction
  - science
  - artificial intelligence
  - dinosaurs
  - extinction
  - nuclear
  - cold war

# Episode particulars
image:
  path: assets/imgs/media/movies/wargames_1983.jpg
  alt: 
links:
  - text: Hardcore History 59 - The Destroyer of Worlds
    urls:
      - text: Hardcore History
        url: 'http://www.dancarlin.com/hardcore-history-59-the-destroyer-of-worlds/'
  - text: Silicon Cowboys
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/silicon-cowboys/id1148235324?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Silicon-Cowboys-Rod-Canion/dp/B01M0F3NNQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/wargames/id268643342?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/WarGames-Matthew-Broderick/dp/B0011EQBOS'
  title: WarGames
  type: movie
  year: 1983
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/wargames_1983.jpg
  number: 76
  guid: 'http://www.decipherscifi.com/?p=1318'
  media:
    audio:
      audio/mpeg:
        content_length: 34668544
        duration: '48:08'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/WarGames_--_Joe_Ruppel_--_decipherSciFi.mp3'
---
#### Cold War

The 50s, the 60s, and the 80s and the climate at the time of the film. Star Wars (but not _that_ Star Wars).

#### Weapons to End War

Gatling and the Gatling Gun, Alfred Nobel and dynamite, Tesla and his particle beams, and.... nuclear weapons?

#### Early-80s Computing Hardware

Text-To-Speech. Voder at the 1939 World's Fair. Matthew Broderick's computer. WarGames computer graphics and rendering radar screens.

{% youtube "https://www.youtube.com/watch?v=0rAyrmm7vv0" %}

#### Machine Learning

Teaching computers to teach themselves. The point where this movie steps into scifi territory.

#### Dinosaurs

Dinosaur extinction hypotheses at the time of the film. Falken is a psychopath.
