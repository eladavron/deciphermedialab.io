---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-12-12 06:35:46+00:00
description: 'Lotsa love for this movie. And then some science? AI, cyborgs, algorithmic girlfriends, solar collector technology, slavery. Worldbuilding backstories, more'
layout: podcast_post
permalink: /decipherscifi/blade-runner-2049-episode-120
redirect_from:
  - /decipherscifi/120
slug: blade-runner-2049-episode-120
title: 'Blade Runner: replicant discrimination, synthetic farming, and virtual girlfriends'
tags:
  - androids
  - data storage
  - digital relationships
  - food production
  - reproduction
  - science
  - science fiction
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/blade_runner_2049_2017.jpg
  alt: 
links:
  - text: 'BLADE RUNNER 2049 - "Black Out 2022" Anime Short'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=rrZk9sSgRyQ'
  - text: 'BLADE RUNNER 2049 - "2036: Nexus Dawn" Short'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=UgsS3nhRRzQ'
  - text: 'BLADE RUNNER 2049 - "2048: Nowhere to Run" Short'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=aZ9Os8cP_gg'
  - text: 'Blade Runner: "androids," the AI control problem, and brain design'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/105'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/blade-runner-2049/id1288813671?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Blade-Runner-2049-Ryan-Gosling/dp/B0765Q45NX9'
  title: Blade Runner 2049
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/blade_runner_2049_2017.jpg
  number: 120
  guid: 'https://decipherscifi.com/?p=3823'
  media:
    audio:
      audio/mpeg:
        content_length: 53358592
        duration: '01:03:30'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Blade_Runner_2049_--_--_decipherSciFi.mp3'
---
#### Replicant discrimination

Is racism the right word? Hate and discrimination against replicants. Replicant slavery.

#### Synthetic farming

Whatever that means. But in the meantime, let's eat some grubs okay? The benefits of cricket and grub farming. Necessity breeds insects.

#### Solar concentrators

Hey these are neat
{% youtube "https://www.youtube.com/watch?v=LMWIgwvbrcM" %}

#### Replicant birth

Pooped out of the goo shoot. Like real life. And then replicants _replicating themselves_.

#### Space settlement

Thank you creepy Jared Leto. Tyrell's original mission versus the cohesive effort lead by creepy Leto.

#### Digital Girlfriend

Narrow v general artificial intelligence. Awesome magic projector technology.

#### Data storage

DNA or other biological data storage. Holographic storage.
