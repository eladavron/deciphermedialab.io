---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-06-03T20:23:10-04:00
description: 'Fitness. Modern AAA gaming production scales. Unreal engine real-time on movie sets. John Carmack. SpaceX dragon mission. Movie languages and storytelling POV. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/240
slug: isolated-bs-hip-thrusts-dragons-linguistic-pov
title: 'Isolated BS: hip thrusts, Dragons, and linguistic POV'
tags:
  - fitness
  - nostaglia
  - programming
  - science
  - science fiction
  - space
  - video games

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_4.jpg
  alt: 'The Dragon Crew browsing Decipher Media (CC-BY-2.0) based on NASA KSC-20180830-PH_SPX01_0004 (CC-0)'
links:
  - text: 'The Hunt For Red October - Learning to Love the Soviets by Patrick Willems'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=2A2qBcjb6Ic'
  - text: 'Masters of Doom by David Kushner'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Masters-Doom-Created-Transformed-Culture-ebook/dp/B000FBFNL0'
      - text: iTunes
        url: 'https://books.apple.com/us/book/masters-of-doom/id420345142?mt=11&app=itunes&at=1001l7hP'
  - text: 'Upload - for the next episode!'
    urls:
      - text: Prime Video
        url: 'https://www.amazon.com/Upload/dp/B0858YGKZ4'
  - text: 'Weekly Free Games'
    urls:
      - text: Epic Games Store
        url: 'https://www.epicgames.com/store/en-US/free-games'
  - text: 'Starting Strength by Makr Rippetoe'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Starting-Strength-Basic-Barbell-Training/dp/0982522738'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_4.jpg
  guid: isolated-bs-hip-thrusts-dragons-linguistic-pov
  number: 240
  media:
    audio:
      audio/mpeg:
        content_length: 34543400
        duration: '41:04'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/isolated_bs_4.final.mp3'

---
# Fitness

Standing desks are great and all, but the real flexibility innovation is not sitting in a car for hours every day. Inventing the "standing car." Hip adductors like steel bands.

# Gaming

Just Dance.Video game bodily control abstractions. DDR. GTA V. Video game scale and [budget madness](https://knowtechie.com/the-most-expensive-video-games-ever-created/). GTA 2 demo nostalgia with co-bff [Joe]({% link _people/joe_ruppel.md %}).

# John Carmack

Polymath went from game engines to cars to actual rockets to VR to AGI, and we wish him great luck on that last one. Doom nostalgia.

# Dragon mission

Yay! Space! Dramatically lowering the cost of space travel. Fancy new flight suits. Are touch screens a good idea? Space travel automation expectations.

# Language and POV

Enjoying the early fight and chase scenes in [Extraction](https://www.netflix.com/title/80230399). The use of language and subtitles for storytelling. [Video](https://youtube.com/watch?v=2A2qBcjb6Ic).
