---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-04-25 07:45:53+00:00
description: 'Skepticism and critical thinking training via magic, CPR history, Tesla and Edison and a call-in about common misconceptions, electricity, obsession, more'
layout: podcast_post
permalink: /decipherscifi/the-prestige-episode-87
redirect_from:
  - /decipherscifi/87
slug: the-prestige-episode-87
title: 'The Prestige: magic as skeptical training, Tesla, and taming electricity'
tags:
  - electromagnetism
  - illusion
  - magic
  - nikola tesla
  - science
  - science fiction
  - skepticism
  - thomas edison

# Episode particulars
image:
  path: assets/imgs/media/movies/the_prestige_2006.jpg
  alt: Two magic dudes and whispy smoke lady in the middle, Prestige movie backdrop
links:
  - text: The Prestige - Hiding in Plain Sight by nerdwriter
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=d46Azg3Pm4c'
  - text: The Trouble with Teleporters by CGP Grey
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=nQHBAdShgYI'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-prestige/id209960866?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Prestige-Hugh-Jackman/dp/B003QS67F0'
  title: The Prestige
  type: movie
  year: 2006
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_prestige_2006.jpg
  number: 87
  guid: 'http://www.decipherscifi.com/?p=1504'
  media:
    audio:
      audio/mpeg:
        content_length: 38705152
        duration: '46:04'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Prestige_--_--_decipherSciFi.mp3'
---
#### This movie is so good!

Just a warning: we really think you should watch this before listening to the episode. It's so good!

#### Magic as skeptical training

Taking advantage of human intuition. Taking advantage of our sensory aparatus. Inoculating against credulity and being fooled.

#### Penn & Teller

Excellent showmen who have been at it forever and who are just great. They are "honest liars" and a lot of their media is great training in cognitive biases.

#### Saving people from drowning

Attempts to derive scientific guidelines for chest compressions and mouth-to-mouth resuscitation to save drowning victims might go back further than you'd expect.

#### Tesla

David Bowie as the perfect dandy to play the rather dandy-ish Tesla.

{% youtube "https://www.youtube.com/watch?v=b8kZo2xByPY" %}

Modern railguns might be in a similar vein to Tesla's "Death Ray"

#### Taming electricity

The wonderment of the period. Misconceptions about the rivalry between Tesla and Edison.

#### Dedication and obsession and sacrifice

The difference between simple and easy. Going over the top to solve a problem with technology where it isn't necessary.
