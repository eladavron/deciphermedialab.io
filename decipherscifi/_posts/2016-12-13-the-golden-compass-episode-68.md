---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-12-13 07:02:29+00:00 
description: 'Golden Compass! A tiny bit scifi and a bunch of fantasy. Polar bear fights, dark matter, the many-worlds hypothesis, the mind-body problem, more' 
layout: podcast_post
permalink: /decipherscifi/the-golden-compass-episode-68
redirect_from:
  - /decipherscifi/68
slug: the-golden-compass-episode-68 
title: 'The Golden Compass: scifi fantasy, polar bear evolution, and dark matter dust' 
tags:
  - science fiction
  - science
  - parallel worlds
  - religion
  - magic
  - dualism
  - mind uploading
  - polar bears

# Episode particulars
image:
  path: assets/imgs/media/movies/the_golden_compass_2007.jpg
  alt: Little girl standing with a warrior polar bear in totally sweet golden armor Golden Compass movie backdrop
links:
  - text: His Dark Materials By Philip Pullman
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/his-dark-materials-omnibus/id468742387?mt=11'
      - text: Amazon
        url: 'https://www.amazon.com/Dark-Materials-Omnibus-Philip-Pullman-ebook/dp/B005WBBYXO'
  - text: Does Dark Matter Break Physics? by PBS Spacetime
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=z3rgl-_a5C0'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-golden-compass/id279588787?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Golden-Compass-Nicole-Kidman/dp/B0013TO3HS'
  title: The Golden Compass
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_golden_compass_2007.jpg
  number: 68
  guid: 'http://www.decipherscifi.com/?p=1207'
  media:
    audio:
      audio/mpeg:
        content_length: 35423173
        duration: '49:11'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Golden_Compass_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/the_golden_compass.jpg alt: "Decipher SciFi hosts as polar bear and little girl from The Golden Compass" caption: "Chris' favorite Decipher SciFi selfie ❤️" %}

#### Production

Issues before, issues during, issues in the final cut. So many issues! How the movie lost much of the depth of the book.

#### Scifi vs Fantasy

Sciencey veneer over a big pile of fantasy. Maybe [steampunk](http://io9.gizmodo.com/329918/golden-compass-is-steampunk-not-fantasy)?

#### Dark Matter

Recognizing that dark matter makes no sense as an explanation for dust. Considering the public conception of dark matter way back when the book was written. Discovery and modern evidence. [Bullet cluster](https://en.wikipedia.org/wiki/Bullet_Cluster#Significance_to_dark_matter). Baryonic matter. Dark energy or the cosmological constant.

#### The Mind-Body Problem

Familiars and daemons and souls. Dualism. Consciousness uploading. Metaknowledge.

#### Polar Bears

Polar bear evolution. Shrinking habitat. Hollow hairs and skin color and other adaptations. Blubber and keeping cool.

#### Parallel Universes

Svalbard! Aurora. The many-worlds interpretation.
