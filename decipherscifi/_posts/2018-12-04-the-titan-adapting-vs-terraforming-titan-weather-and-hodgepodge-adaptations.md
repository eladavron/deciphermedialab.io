---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-12-04 08:45:18+00:00
description: 'Titan: the second best candidate for life within our solar system. The difficulty of terraforming. Homniforming. Evil scientist Dr. Space Moreau.'
layout: podcast_post
permalink: /decipherscifi/the-titan-adapting-vs-terraforming-titan-weather-and-hodgepodge-adaptations
redirect_from:
  - /decipherscifi/170
slug: the-titan-adapting-vs-terraforming-titan-weather-and-hodgepodge-adaptations
title: 'The Titan: adapting vs terraforming Titan, individual adaptations'
tags:
  - evil scientist
  - saturn
  - science
  - science fiction
  - space
  - space settlement
  - terraforming
  - titan
  - transhumanism
  - weather

# Episode particulars
image:
  path: assets/imgs/media/movies/the_titan_2018.jpg
  alt: 'A guy, and a girl, and some space. The Titan movie backdrop'
links:
  - text: "What About a Mission to Titan? It's Time to Explore Saturn's Largest Moon by Fraser Cain"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=rzg78ySwIn4'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/80148210'
  title: The Titan
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_titan_2018.jpg
  number: 170
  guid: 'https://decipherscifi.com/?p=8411'
  media:
    audio:
      audio/mpeg:
        content_length: 34882228
        duration: '41:30'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Titan_--_--_Decipher_Scifi.mp3'
---

#### Titan

The most interesting moon 'round Saturn! Viewing the moon Titan by eye or by telescope. Protip: bincolulars are actually just two tiny telescopes.

#### Saturn

The Saturnian _hexagonal_ storm vs the larger Jovian storm. Latitudinal wind gradients.

#### Adaptation

Adapting the world to our needs. The possibility of adapting ourselves to a world. Inhospitality. Ethiopian, Tibetan, and Andean elevation adaptation. Andean vampires?

#### Titan Habitability

Atmosphere. Temperature. Breathability. Tidal locking and avoiding tidal locking with Jupiter. Frozen and liquid water! Methane oceans. No shortage of atmospheres in our solar system, but we're short on good ones.

#### Probing titan

[The Huygens probe](https://en.wikipedia.org/wiki/Huygens_(spacecraft)). Proposed future Titan exploration projects.Drones, submarines, floatydrones, sinkydrones, etc.

#### Weather

Titanian weather gets bigs! Large methane rainstorms, but really only after decaded or centuries of drought. Energy availability so far from the sun.

#### Hominoforming

Who needs oxygen? Apnea training irl. Suck-tubes. Eventually breathing methane. Cat eyes vs shark eyes and which is best for a flying lizardman.

#### Evil scientist

Space Moreau, or Space Mengele? Space Merengele? Individual plasticity. Creating the ultimate MMA alien creature thing. Modeling after Bas Rutten. The final mutation hogdepodge. Bat-lats. Human flight on Titan.
