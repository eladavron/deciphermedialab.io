---
layout: person_page
person_data:
  name: Christopher Peterson
  data_name: christopher_peterson
  image_path: assets/imgs/people/christopher_peterson.png
  title: Podcaster etc
  bio: 'Geek nerd dad person and Decipher Media co-founder'
  websites:
    - name: ChrisPeterson.info
      url: 'https://chrispeterson.info'
  social:
    - name: twitter
      url: https://www.twitter.com/cspete
    - name: instagram
      url: https://www.instagram.com/c_s_peterson
  roles:
    - show: decipherhistory
      role: host
    - show: decipherrpg
      role: guest
    - show: decipherscifi
      role: host
---
