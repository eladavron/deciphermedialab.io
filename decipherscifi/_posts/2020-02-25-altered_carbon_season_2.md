---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2020-02-25T02:45:00-05:00
description: 'Trying to review things. Memory retrieval cues. Facial recognition heuristics. Facial reproduction. AI xenoarchaeology. Etc!'
layout: podcast_post
permalink: /decipherscifi/altered-carbon-season-2-memory-facial-recognition-and-xenoarchaeology
redirect_from:
  - /decipherscifi/230
slug: altered-carbon-season-2-memory-facial-recognition-and-xenoarchaeology
title: 'Altered Carbon Season 2: memory, facial recognition, and xenoarchaeology'
tags:
  - aliens
  - artificial intelligence
  - facial recognition
  - memory
  - science
  - science fiction
  - transhumanism
  - xenoarchaeology

# Episode particulars
image:
  path: assets/imgs/media/tv/altered_carbon_2018_season_2.jpg
  alt: 'Altered Carbon season 2 re-sleeved Takeshi'
links:
  - text: 'Altered Carbon: Zoltan Istvan on transplant immortality, identity, and consciousness uploading'
    urls:
      - text: 'Decipher Scifi'
        url: 'https://decipherscifi.com/128'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/80097694'
  title: Altered Carbon
  type: tv
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/altered_carbon_2018_season_2.jpg
  number: 230
  guid: 'https://decipherscifi.com/?p=10941'
  media:
    audio:
      audio/mpeg:
        content_length: 25414301
        duration: '30:09'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/altered_carbon_season_2.final.mp3'

---
#### A review

We don't normally do this, but...

#### Memory

Priming. Retrieval cues. Memory encoding, sotrage, and retrieval. Context-dependent memory. Getting drunk and finding your keys. Digital memory retrieval Facial recognition heuristics. Recognition vs reproduction.

#### Xenoarchaeology

Living amongst the runes. More AI! Deciphering alien technology. 
